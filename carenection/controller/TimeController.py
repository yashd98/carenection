from re import match

from PyQt5.QtCore import *


class TimeController:
    def __init__(self, view):
        super(TimeController, self).__init__()
        self.view = view
        global timer
        timer = QTimer()
        self.date = QDateTime()
        timer.timeout.connect(self.fetch_system_time)
        timer.start(1000)

    def fetch_system_time(self):
        date_time = self.date.currentDateTime()
        current_time = date_time.time().toString("h:mm AP")
        current_day = self.getDay(date_time.date().dayOfWeek())
        current_time_day = current_day + ", " + current_time
        self.view.mainView.setAppTime(current_time_day)

        if date_time.date() == self.view.calenderFrame.date_after_week.date():
            self.view.calenderFrame.updateCalendarFrameContainer()

    def getDay(self, dayOfWeek):
        if dayOfWeek == 7:
            return "Sunday"
        elif dayOfWeek == 1:
            return "Monday"
        elif dayOfWeek == 2:
            return "Tuesday"
        elif dayOfWeek == 3:
            return "Wednesday"
        elif dayOfWeek == 4:
            return "Thursday"
        elif dayOfWeek == 5:
            return "Friday"
        elif dayOfWeek == 6:
            return "Saturday"
        else:
            raise Exception("Day of week integer falls outside of range")
