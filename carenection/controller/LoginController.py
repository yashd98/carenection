
class LoginController:
    def __init__(self, controller, view):
        super(LoginController, self).__init__()
        self.controller = controller
        self.view = view

    def loginClickEvent(self):
        self.view.main_stacked_widget.setCurrentIndex(2)
        self.controller.picture_frame_controller.startSlideShow()

    def createClickEvent(self):
        self.view.main_stacked_widget.setCurrentIndex(1)

