from controller.CalendarAndSchedulerController import CalendarAndSchedulerController
from controller.CreateAccountController import CreateAccountController
from controller.LogbookController import LogbookController
from controller.AdminController import AdminController
from controller.LoginController import LoginController
from controller.PictureFrameController import PictureFrameController
from controller.SettingController import SettingController
from controller.TimeController import TimeController
from controller.NotificationController import NotificationController
from controller.UserController import UserController
from models.LogbookModel import LogbookModel
from models.Model import Model
from controller.MainViewController import MainViewController
from models import QLabelClickable
from view.NotificationBoxVeiw import ErrorNotificationDialog, NotificationBoxFactory
from view.View import View
from PyQt5.QtWidgets import *
from PyQt5 import QtCore
import sys


class Controller:
    def __init__(self):
        super(Controller, self).__init__()
        self.calendar_scheduler_controller = CalendarAndSchedulerController()

        # Connection between Controller and View
        # Keep reference of the view and also have view keep reference
        # to this controller by passing in controller as parameter to View
        self.view = View(self)


        # Connection between Controller and Model (One way connection)
        # Keep reference of the model
        # Model holds all the references to sub-models,
        # one can get access to it by invoking something like 'self.model.Person'
        self.model = Model()

        # Instantiate all Controllers (LoginController, LogbookController, etc.)
        # Keep references to all controllers
        self.time_controller = TimeController(self.view)
        self.login_controller = LoginController(self, self.view)
        self.main_controller = MainViewController(self.view)
        self.admin_controller = AdminController(self.view)
        self.logbook_controller = LogbookController()
        self.picture_frame_controller = PictureFrameController(self.view)
        self.setting_controller = SettingController(self.view)
        self.notification_controller = NotificationController(self.view)
        self.user_controller = UserController()
        self.create_account_controller = CreateAccountController(self.view)


        self.patientId = "XXXXX"
        self.adminId = "YYYYY"

        # TODO: Please add controllers above for each controller that you are working on

    # All these methods below are connected with each event in the View
    # These methods are basically when view sends requests to controller for an event
    # One of these method will invoke after an request and respective implementation will be run and that method will also update view in the end
    # TODO: Please add respective methods for each controller you specified above, and
    #  communicate with the person who's working on your controllers' UI to get idea input parameters for your method(s)

    #
    # TODO: Also please separate out the methods by each class who is using them
    #

    def loginActionListener(self, username, password):
        patientId = self.user_controller.loginPatient(username, password)
        if self.patientId == 0:
            self.create_notification_pop_up("Incorrect Username/Password")
        elif patientId == 123:
            self.create_notification_pop_up("DB is Offline")
        else:
            self.patientId = patientId
            self.login_controller.loginClickEvent()

    def createAccountActionListener(self, username, password, name, phone_number, email, room_number = "room_num", medical_condition = "medical_condition"):
        print("In controller")
        createAccountResponse= self.user_controller.registerPatient(username, password, name, phone_number, email, room_number, medical_condition)
        if  createAccountResponse == 1:
            self.create_notification_pop_up("Patient Registered Successfully", "success")
        elif createAccountResponse == 123:
            self.create_notification_pop_up("DB is Offline")
        else:
            self.create_notification_pop_up("Patient Registration Unsuccessful")
        self.create_account_controller.createClickEvent()


    def backActionListener(self):
        self.create_account_controller.backClickEvent()

    def createActionListener(self):
        self.login_controller.createClickEvent()

    def pictureFrameListener(self):
        self.main_controller.pc_btnClick()

    def videoFrameListener(self):
        self.main_controller.vf_btnClick()

    def calenderFrameListener(self):
        self.main_controller.cf_btnClick()

    def exit_listener(self):
        self.main_controller.exit_btnClick()

    def admin_listener(self):
        self.admin_controller.adminClickEvent()

    def exit_admin_listener(self):
        self.admin_controller.exitClickEvent()

    def admin_login_action_listener(self, username, password):
        adminId = self.user_controller.loginAdmin(username, password)
        if self.adminId == 0:
            self.create_notification_pop_up("Incorrect Username/Password")
        elif adminId == 123:
            self.create_notification_pop_up("DB is Offline")
        else:
            self.adminId = adminId
            self.view.main_stacked_widget.setCurrentIndex(4)

    def uploadPicture(self, filePath):
        self.setting_controller.upload(filePath)
        self.picture_frame_controller.createPictureModels()
        print("picture frame updated")

    def updateConfig(self, picture, calendar):
        self.setting_controller.updateConfig(picture,calendar)

    # For Handling requests to Logbook Controller
    def logbook_listener(self, model, eventtype):
        # print("Hello ", type(model), " ", model.to_str_repr())

        if eventtype == "add":
            #error check
            self.logbook_controller.add_log(model)
        elif eventtype == "remove":
            #error check
            self.logbook_controller.rem_log(model)
        elif eventtype == "edit":
            #error check
            self.logbook_controller.edit_log(model)

        # logtable.setItem(rowpos, 0, QTableWidgetItem("Hello"))

        # self.logbook_controller.log(params)
        # Update UI to display Success or Failure depending on the result of above line

    def cal_adm_dt_sel_listener(self,date):
        return self.calendar_scheduler_controller.fetch_events_on_date(3,date) # Return all models fetched from DB

    def cal_adm_add_event_listener(self, pid, event_start, event_end, event_description, event_name):
        return self.calendar_scheduler_controller.create_event(pid,event_start, event_end, event_name, event_description, 10)

    def delete_event_listener(self, event):
        self.calendar_scheduler_controller.delete_event(event)

    def keypress_listener(self,event):
        key = event.key()
        # print("Key: ", event.key())
        if key == QtCore.Qt.Key.Key_Enter:
            # perform search from db

            return True
        else:
            return False

    #ERROR AND SUCCESS DIALOG
    def create_notification_pop_up(self, message, msg_type='error'):
        self.notification_controller.create_notification_box(message, msg_type)
        # That's how you call the error and success dialog
        # cont.create_notification_pop_up("You have sucessfully logged in", 'success')
        # cont.create_notification_pop_up("That name is not in the database", 'error')

if __name__ == '__main__':
    # Initial Startup

    app = QApplication(sys.argv)
    cont = Controller()
    cont.view.showMaximized()
    cont.view.setWindowTitle("Carenections")
    cont.view.show()
    sys.exit(app.exec_())
