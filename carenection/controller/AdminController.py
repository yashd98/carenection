class AdminController:
    def __init__(self, view):
        super(AdminController, self).__init__()
        self.view = view

    def adminClickEvent(self):
        self.view.main_stacked_widget.setCurrentIndex(3)

    def exitClickEvent(self):
        self.view.main_stacked_widget.setCurrentIndex(2)