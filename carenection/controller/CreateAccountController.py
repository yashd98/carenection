
class CreateAccountController:
    def __init__(self, view):
        super(CreateAccountController, self).__init__()
        self.view = view

    def createClickEvent(self):
        self.view.main_stacked_widget.setCurrentIndex(0)

    def backClickEvent(self):
        self.view.main_stacked_widget.setCurrentIndex(0)

