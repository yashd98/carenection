from PyQt5.QtWidgets import *
from view import NotificationBoxVeiw


class NotificationController:

    def __init__(self, view):
        self.view = view

    def create_notification_box(self, message, msg_type='error'):
        # create notification box
        if msg_type == 'error':
            new_notification_pop_up = self.view.notificationBoxFactory.createErrorNotification(self.view, message)
            new_notification_pop_up.show()
        elif msg_type == 'success':
            new_notification_pop_up = self.view.notificationBoxFactory.createSuccessNotification(self.view, message)
            new_notification_pop_up.show()

