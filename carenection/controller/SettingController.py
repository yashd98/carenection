from PyQt5.QtWidgets import *
from PyQt5 import QtCore
import shutil

from pathlib import *
import re
import os

class SettingController:
    def __init__(self, view):
        super(SettingController, self).__init__()
        self.view = view

    def upload(self,filePath):
        fileName=re.split("\/", filePath[0])
        target = str(Path(__file__).parent.parent / f"view/pf_images/{fileName[len(fileName)-1]}")
        shutil.move(filePath[0], target)
        print("File moved")
    def updateConfig(self,picture,calendar):
        file=open(str(Path(__file__).parent.parent / f"view/user_config/config.txt"),"w")
        file.write(str(picture)+","+str(calendar))
        file.close()
