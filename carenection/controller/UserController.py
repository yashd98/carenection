# Only purpose is to write logs
import json

from PyQt5.QtWidgets import *
from models.PatientModel import Patient
from services.backend_service import backend_service
from services.encryption_service import AuthKeyEncoder
class UserController:from models.AdminModel import Admin
from models.PatientModel import Patient
from models.AdminModel import Admin



class UserController:
    def __init__(self):
        super(UserController, self).__init__()

    def registerPatient(self, username, password, name, phone_number, email, room_number, medical_condition):
        try:
            patient = Patient(username, password, name, phone_number, email, room_number, medical_condition, 0)
            if patient.is_valid():
                req = backend_service.register_patient(username, password, name, phone_number, email, room_number, medical_condition)
                if req.status_code == 200:
                    authKey = json.loads(req.text)["authkey"]
                    AuthKeyEncoder(authKey, "encrypted_file")
                    #print("Successful Login")
                    return 1
                else:
                    #print("Incorrect username or password. Please try again")
                    return None
        except:
            return 123
        #error: if username already exists,
        # add entered data to the database
        #set as either patient ot admin model
        #if not account.is_valid:
        #return "All fields not valid, please try again"
        #add user, password, email, to DB
        #if successful return success + redirect to main view


    def loginPatient(self, username, password):
        # DB will return an auth token that will stay the same until the user logs out
        # send HTTP GET request for the user and password
        # will return a patient id
        authkey = AuthKeyEncoder.get_auth_key()
        try:
            request = backend_service.login_patient(username, password, authkey)


            if request.status_code == 200:
                patientId = json.loads(request.text)["pid"]
                self.Controller.setPatientId(patientId)
                print("Successful Login")
                return patientId

            else:
                print("Incorrect username or password. Please try again")
                return 0
        except:
            return 123


        #if HTTP request == user && password
        #   store auth token until log out
        #   redirect to main view
        #else return incorrect info, try again



    def registerAdmin(self, username, password, name, phone_number, email):

        admin = Admin(username, password, name, phone_number, email)
        if admin.is_valid():
            res = backend_service.register_caregiver(username, password, name, phone_number, email)
            if res.status_code == 200:
                print("Successful Registration")
                return True
            else:
                print("Failed to Register")
                return False



    def loginAdmin(self, username, password):
        try:
            response = backend_service.login_admin(username, password)

            if response.status_code == 200:
                adminId = json.loads(response.text) ["admin_id"]
                print("Successful Login")
                return adminId
            else:
                print("Failed to Login")
                return 0
        except:
            return 123







