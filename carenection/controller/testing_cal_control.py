import datetime as dt
import PyQt5.QtCore
from controller.CalendarAndSchedulerController import CalendarAndSchedulerController

c = CalendarAndSchedulerController()
# creating the events list (which will mimic the system db functionality for now)
admin_id = 10; # all of the following events were inputted into system by Admin10 (suppose its a nurse w/ admin auth over multiple patients)
evt1 = c.create_event(1, dt.datetime(2021, 11, 20, 8), dt.datetime(2021, 11, 20, 9), 'Dr appt', 'Appointment with Dr. Brown', admin_id)
evt2 = c.create_event(2, dt.datetime(2021, 11, 20, 12), dt.datetime(2021, 11, 20, 14, 15), 'Visit from John', 'Your son John will visit today', admin_id)
evt3 = c.create_event(2, dt.datetime(2021, 11, 20, 15), dt.datetime(2021, 11, 20, 16), 'Eye exam', 'Eye exam with Dr. Smith', admin_id)
evt4 = c.create_event(1, dt.datetime(2021, 11, 21, 10), dt.datetime(2021, 11, 21, 10, 30), 'Exercise', 'Remember to complete your arm and leg stretches today', admin_id)
evt5 = c.create_event(1, dt.datetime(2021, 11, 28, 22), dt.datetime(2021, 11, 29, 15), 'Visit from Jane', 'Your daughter Jane will visit today', admin_id)
evt6 = c.create_event(1, dt.datetime(2021, 11, 28, 22), dt.datetime(2021, 11, 28, 21), 'Chiro', 'With Dr. X', admin_id)

print('Summary of all events in "db":')
for e in c.events:
    print('\tPatient', e.get_pid(), '-', e.get_event_name().upper(), 'from <', e.get_event_start(), '> to <', e.get_event_end(), '>.')
print('--')

print('All events for patient 2 on 11/21:')
q_date = PyQt5.QtCore.QDate(2021, 11, 21)
fetched_events = c.fetch_events_on_date(2, q_date.toPyDate())
for e in fetched_events:
    print('\tPatient', e.get_pid(), '-', e.get_event_name().upper(), 'from <', e.get_event_start(), '> to <', e.get_event_end(), '>.')
print('(expected to return no events)')
print('--')

print('All events for patient 1 on 11/20:')
q_date = PyQt5.QtCore.QDate(2021, 11, 20)
fetched_events = c.fetch_events_on_date(1, q_date.toPyDate())
for e in fetched_events:
    print('\tPatient', e.get_pid(), '-', e.get_event_name().upper(), 'from <', e.get_event_start(), '> to <', e.get_event_end(), '>.')
print('--')

print('access your last query using the controller attribute last_query')
for e in c.last_query:
    print('\tPatient', e.get_pid(), '-', e.get_event_name().upper(), 'from <', e.get_event_start(), '> to <', e.get_event_end(), '>.')
print('--')

print('All events for patient 1 THIS WEEK:')
fetched_events = c.fetch_events_range(1, dt.datetime.now().date(), dt.datetime.now().date() + dt.timedelta(days=7))
for e in fetched_events:
    print('\tPatient', e.get_pid(), '-', e.get_event_name().upper(), 'from <', e.get_event_start(), '> to <', e.get_event_end(), '>.')
print('--')

print('Deleted event 4 then display all events again:')
c.delete_event(evt4)
for e in c.events:
    print('\tPatient', e.get_pid(), '-', e.get_event_name().upper(), 'from <', e.get_event_start(), '> to <', e.get_event_end(), '>.')
print('--')

print('The following is a string reminder of events for Patient 1 starting at the next hour:')
print(c.fetch_next_hour_notif(1))

