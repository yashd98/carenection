from PyQt5.QtWidgets import *
class MainViewController(QMainWindow):
    def __init__(self, view):
        super(MainViewController, self).__init__()
        self.view = view

    def pc_btnClick(self):
        self.view.mainView.frameContainer.setCurrentIndex(0)

    def vf_btnClick(self):
        self.view.mainView.frameContainer.setCurrentIndex(1)

    def cf_btnClick(self):
        self.view.mainView.frameContainer.setCurrentIndex(2)

    def exit_btnClick(self):
        self.view.main_stacked_widget.setCurrentIndex(0)
