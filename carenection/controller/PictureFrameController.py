from models.PictureModel import PictureModel
from PyQt5.QtCore import *
from pathlib import *
import re

class PictureFrameController:
    def __init__(self, view):
        super(PictureFrameController, self).__init__()
        self.view = view
        self.slideshow = self.view.pictureFrame.getSlideShow()
        self.createPictureModels()

    def createPictureModels(self):
        for path in Path(str(Path(__file__).parent.parent / f"view/pf_images/")).iterdir():
            fName= re.split("\\\\", str(path))
            self.img=PictureModel()
            self.img.addPicture(str(fName[len(fName)-1]))
            self.slideshow.addWidget(self.img.getImg())

        self.slideshow.setSpeed(100)
        self.slideshow.setAnimation(QEasingCurve.Type.Linear)
        self.slideshow.setWrap(True)

    def startSlideShow(self):
        global timer
        timer = QTimer()
        timer.timeout.connect(self.slideshow.slideInNext)
        timer.start(5000)
