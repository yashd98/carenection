import datetime as dt
from models.CalendarEventModel import CalendarEventModel
from services.backend_service import backend_service
import requests
import json

class CalendarAndSchedulerController:
    def __init__(self):
        super(CalendarAndSchedulerController, self).__init__()
        self.events = [] # all events in "db"
        self.last_query = [] # last fetched sublist of events

    def create_event(self, pid, event_start, event_end, event_name, event_description, event_admin):
        # TODO: remove event admin attribute from model and view
        if event_start > event_end:
            raise Exception('Event end time is earlier the start time')
        new_event = CalendarEventModel(pid, event_start, event_end, event_name, event_description, event_admin)
        self.events.append(new_event)
        return new_event


        # rbody = {
        #     "pid": pid,
        #     "event_start": event_start,
        #     "event_end": event_end,
        #     "event_name": event_name,
        #     "event_description": event_description
        # }
        # r = requests.post('', json = rbody)

    def delete_event(self, event):
        self.events.remove(event)

    def fetch_events_on_date(self, pid, fetch_date):
        today_events = [event for event in self.events if event.event_start.date() == fetch_date and event.get_pid() == pid]
        self.last_query = today_events
        return today_events

    def fetch_events_range(self, pid, start_fetch_date, end_fetch_date):
        range_events = [event for event in self.events if
                        start_fetch_date <= event.event_start.date() <= end_fetch_date and
                        event.get_pid() == pid]
        self.last_query = range_events
        return range_events

    def fetch_next_hour_notif(self, pid):
        # this way of implementing notifications does not involve threading.
        # outputs a string containing a reminder of the events starting on the next hour OR empty string
        # e.g. call this function between X:00 and X:59 to get the reminder message for events starting at (X+1):00
        hour_to_notify = dt.datetime.now().replace(minute = 0, second = 0, microsecond= 0) + dt.timedelta(hours = 1)
        message = "Reminder: the following event(s) are scheduled today at " + hour_to_notify.strftime("%I:%M %p") + ":\n"
        count = 0
        for event in self.events:
            if event.event_start == hour_to_notify and event.get_pid() == pid:
                message += event.get_event_name() + "\n"
                count += 1
        if count == 0:
            message = "" # nothing scheduled for the next hour; do not prompt the user.
        return message

