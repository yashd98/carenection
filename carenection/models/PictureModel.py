from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from pathlib import *

class PictureModel(QLabel):
    def __init__(self):
        super(PictureModel, self).__init__()
        self.img = QLabel()

    def addPicture(self, name):
        file_path = str(Path(__file__).parent.parent / f"view/pf_images/{name}")
        self.img.setPixmap(QPixmap(file_path))
        self.img.setScaledContents(True)

    def getImg(self):
        return self.img