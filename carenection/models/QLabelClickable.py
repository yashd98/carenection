from PyQt5.QtWidgets import *
from PyQt5.QtCore import *


class QLabelClickable(QLabel):
    clicked = pyqtSignal(str)

    def __init__(self):
        super(QLabelClickable, self).__init__()

    def mousePressEvent(self, event):
        self.latest = "click"

    def mouseReleaseEvent(self, event):
        if self.latest == "click":
            QTimer.singleShot(QApplication.instance().doubleClickInterval(),
                              self.performSingleClickAction)
        else:
            self.clicked.emit(self.latest)

    def mouseDoubleClickEvent(self, event):
        self.latest = "Double Click"

    def performSingleClickAction(self):
        if self.latest == "Click":
            self.clicked.emit(self.latest)
