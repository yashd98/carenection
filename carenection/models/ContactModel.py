class ContactModel:
    def __init__(self, name, email, rel):
        super(ContactModel, self).__init__()
        self.name = name
        self.email = email
        self.relation = rel

    def setName(self, name):
        self.name = name

    def setEmail(self, email):
        self.email = email

    def setRelation(self, rel):
        self.relation = rel

    def getName(self):
        return self.name

    def getEmail(self):
        return self.email

    def getRelation(self):
        return self.relation

    def getData(self):
        return {"Name": self.name, "Email": self.email, "Relation": self.relation}

    def __str__(self):
        return self.getData().__str__()