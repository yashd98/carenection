class CalendarEventModel:
    def __init__(self, pid, event_start, event_end, event_name, event_description, event_admin):
        super(CalendarEventModel, self).__init__()
        self.pid = pid
        self.event_start = event_start
        self.event_end = event_end
        self.event_name = event_name
        self.event_description = event_description
        self.event_admin = event_admin
        # go back and check if this matches the current table schema

    def get_pid(self):
        return self.pid

    def get_event_start(self):
        return self.event_start

    def get_event_end(self):
        return self.event_end

    def get_event_name(self):
        return self.event_name

    def get_event_description(self):
        return self.event_description

    def get_event_admin(self):
        return self.event_admin

    def set_pid(self, new_pid):
        self.pid = new_pid

    def set_event_start(self, new_start):
        self.event_start = new_start

    def set_event_end(self, new_end):
        self.event_end = new_end

    def set_event_name(self):
        return self.event_name

    def set_event_description(self, new_description):
        self.event_description = new_description

    def set_event_admin(self, new_admin):
        self.event_admin = new_admin
