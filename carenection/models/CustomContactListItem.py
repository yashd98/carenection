from PyQt5.QtCore import QSize
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtWidgets import *
from PyQt5.Qt import Qt
import os


class CustomContactListItem(QWidget):
    def __init__(self, model, mode):
        super(CustomContactListItem, self).__init__()
        self.model = model
        self.mode = mode # 1 - edit, 0 - read
        self.namelbl = QLabel(f'{self.model.getName()} [{self.model.getRelation()}]')
        self.btnedit = QPushButton()
        self.btnremove = QPushButton()
        self.create_widgets()
        # print("Hello")
        style="""
            QLabel{
                color: white;
                font-size: 25px;
                background-color: transparent;
            }
        """
        self.setStyleSheet(style)

    def create_widgets(self):
        hlayout = QHBoxLayout(self)

        hlayout.addWidget(self.namelbl)

        resize_height = self.btnremove.sizeHint().height() * (1 + 0.50)

        curdir = os.path.dirname(os.getcwd())
        pmap_edit = QPixmap(os.path.join(curdir, "view/icons/edit_2.png"))
        self.btnedit.setFixedSize(QSize(resize_height, resize_height))
        self.btnedit.setIcon(QIcon(pmap_edit))
        self.btnedit.setToolTip("Edit the current contact")

        if(self.mode):
            hlayout.addWidget(self.btnedit)

        pmap_rem = QPixmap(os.path.join(curdir, "view/icons/delete.png"))
        self.btnremove.setFixedSize(QSize(resize_height, resize_height))
        self.btnremove.setIcon(QIcon(pmap_rem))
        self.btnremove.setToolTip("Remove the current contact")

        if(self.mode):
            hlayout.addWidget(self.btnremove)

    def refreshContactLabel(self):
        self.namelbl.setText(f'{self.model.getName()} [{self.model.getRelation()}]')