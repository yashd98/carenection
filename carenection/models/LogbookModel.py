import datetime as dt


class LogbookModel:
    def __init__(self, patientid, administrator, meal="N/A", medicine="N/A"):
        self.patid = patientid
        self.meal = meal
        self.medicine = medicine
        self.admin = administrator
        self.datetime = dt.datetime.now()  # set date time to the time of Logging, later look at adding timezones

    def set_patient_id(self, patientid):
        self.patid = patientid

    def set_meal(self, meal):
        self.meal = meal

    def set_medicine(self, medicine):
        self.medicine = medicine

    def set_admin(self, administrator):
        self.admin = administrator

    def get_patient_id(self):
        return self.patid

    def get_meal(self):
        return self.meal

    def get_medicine(self):
        return self.medicine

    def get_admin(self):
        return self.admin

    def get_date(self):
        return self.datetime.date()

    def get_time(self):
        return self.datetime.time()

    def get_datetime(self):
        return self.datetime

    def get_datetime_str(self):
        return self.datetime.strftime("%Y-%m-%d %H:%M:%S")

    def to_str_repr(self):
        return f'PatientID: {self.patid}, Administrator: {self.admin}, Medicine: {self.medicine}, Meal: {self.meal}, ' \
               f'Time of Log: {self.get_datetime_str()}'
