from models.LogbookModel import LogbookModel


def get_logbook_model(patientid, administrator, meal="N/A", medicine="N/A"):
    return LogbookModel(patientid, administrator, meal, medicine)


class Model:
    def __init__(self):
        super(Model, self).__init__()

    # Instantiate all Models (this includes PersonModel, LogbookModel, ContactModel, etc.)

