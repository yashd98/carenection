from PyQt5.QtCore import QSize
from PyQt5.QtWidgets import *
from PyQt5 import Qt


class CustomDisplayContactLabel(QWidget):
    def __init__(self):
        super(CustomDisplayContactLabel, self).__init__()
        self.empty_wid = QFrame()
        self.main_lay = QVBoxLayout(self)
        self.main_lay.setContentsMargins(20, 20, 20, 20)

        self.namelbl = QLabel(self)
        self.emaillbl = QLabel(self)
        self.relationlbl = QLabel(self)
        self.imagelbl = QLabel(self)

        self.nametxt = QLabel(self)
        self.emailtxt = QLabel(self)
        self.relationtxt = QLabel(self)

        self.create_widgets()
        height = self.height()
        self.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed))
        self.setFixedSize(QSize(height, height))

        style = """
            QFrame{
                border: 4px solid white;
                border-radius: 10px;
            }
            QLabel{
                font-size: 152px;
                color: white;
            }
            QLabel#lbl{
                color: white;
                border: no-border;
                font-size: 20px;
                font-weight: bold;
                text-decoration: underline;
            }
            QLabel#txt{
                color: white;
                border: no-border;
                font-size: 20px;
            }
        """
        self.empty_wid.setStyleSheet(style)
        self.main_lay.addWidget(self.empty_wid)
        # print(self.size().height())

    def create_widgets(self):
        hlayout_name = QHBoxLayout()
        hlayout_name.setContentsMargins(0, 0, 0, 0)
        hlayout_name.setSpacing(0)

        hlayout_email = QHBoxLayout()
        hlayout_email.setContentsMargins(0, 0, 0, 0)
        hlayout_email.setSpacing(0)

        hlayout_relation = QHBoxLayout()
        hlayout_relation.setContentsMargins(0, 0, 0, 0)
        hlayout_relation.setSpacing(0)

        vlayout = QVBoxLayout()
        vlayout.setSpacing(0)
        vlayout.setContentsMargins(50, 5, 50, 5)  # left, top, right, bottom

        # print(self.height()," ", self.height())
        self.imagelbl.setFixedSize(self.height()*0.6, self.height()*0.6)
        self.imagelbl.setAlignment(Qt.Qt.AlignCenter)
        vlayout.addWidget(self.imagelbl, alignment=Qt.Qt.AlignHCenter)

        self.namelbl.setText("Name:")
        self.namelbl.setObjectName("lbl")
        self.nametxt.setObjectName("txt")
        hlayout_name.addWidget(self.namelbl)
        hlayout_name.addWidget(self.nametxt)

        self.emaillbl.setText("Email:")
        self.emaillbl.setObjectName("lbl")
        self.emailtxt.setObjectName("txt")
        hlayout_email.addWidget(self.emaillbl)
        hlayout_email.addWidget(self.emailtxt)

        self.relationlbl.setText("Relation:")
        self.relationlbl.setObjectName("lbl")
        self.relationtxt.setObjectName("txt")
        hlayout_relation.addWidget(self.relationlbl)
        hlayout_relation.addWidget(self.relationtxt)

        vlayout.addLayout(hlayout_name)
        vlayout.addLayout(hlayout_email)
        vlayout.addLayout(hlayout_relation)

        self.empty_wid.setLayout(vlayout)

    def setInitials(self):
        # print(self.nametxt.text().split())
        firstlast = self.nametxt.text().split()
        if len(firstlast) == 1:
            # print("Empty")
            self.imagelbl.setText(firstlast[0][0])
        else:
            self.imagelbl.setText(f'{firstlast[0][0].upper()}{firstlast[1][0].upper()}')

    def setName(self, name):
        self.nametxt.setText(name)

    def setEmail(self, email):
        self.emailtxt.setText(email)

    def setRelation(self, rel):
        self.relationtxt.setText(rel)

    def setData(self, data):
        if len(data) == 3:
            self.setName(data["Name"])
            self.setEmail(data["Email"])
            self.setRelation(data["Relation"])
            self.setInitials()

    def getName(self):
        self.nametxt.text()

    def getEmail(self):
        self.emailtxt.text()

    def getRelation(self):
        self.relationtxt.text()

    def getData(self):  # [name, email, relation]
        return [self.getName(), self.getEmail(), self.getRelation()]

    def hide_widgets(self):
        self.namelbl.hide()
        self.nametxt.hide()
        self.emaillbl.hide()
        self.emailtxt.hide()
        self.relationlbl.hide()
        self.relationtxt.hide()
        self.imagelbl.hide()

    def show_widgets(self):
        self.namelbl.show()
        self.nametxt.show()
        self.emaillbl.show()
        self.emailtxt.show()
        self.relationlbl.show()
        self.relationtxt.show()
        self.imagelbl.show()
