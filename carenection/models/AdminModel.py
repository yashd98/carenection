# this file represents the admin table in the database
# all fields in __init__ are table headers

class Admin:
    def __init__(self, username, name, email, password, phone):
        super(Admin, self).__init__()
        self.username = username
        self.name = name
        self.email = email
        self.password = password
        self.phone = phone

    def get_username(self):
        return self.username

    def set_username(self, username):
        self.username = username

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_email(self):
        return self.email

    def set_email(self, email):
        self.email = email

    def get_password(self):
        return self.password

    def set_password(self, password):
        self.password = password

    def get_phone(self):
        return self.phone

    def set_phone(self, phone):
        self.phone = phone

    def is_valid(self):
        # checks if any of the fields are empty
        if not self.username or not self.name or not self.email or not self.password or not self.phone:
            return False
        else:
            return True

