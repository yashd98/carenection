# this file represents the patient table in the database
# all fields in __init__ are table headers

class Patient:
    def __init__(self, username, password, name, phone, email, room, med, patientid):
        super(Patient, self).__init__()
        self.username = username
        self.password = password
        self.name = name
        self.phone = phone
        self.email = email
        self.room = room
        self.med = med
        self.patientid = patientid

    def get_username(self):
        return self.username

    def set_username(self, username):
        self.username = username

    def get_password(self):
        return self.password

    def set_password(self, password):
        self.password = password

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_phone(self):
        return self.phone

    def set_phone(self, phone):
        self.phone = phone

    def get_email(self):
        return self.email

    def set_email(self, email):
        self.email = email

    def get_room(self):
        return self.room

    def set_room(self, room):
        self.room = room

    def get_med(self):
        return self.med

    def set_med(self, med):
        self.med = med

    def get_patientid(self):
        return self.patientid

    def set_patientid(self, patientid):
        self.patientid = patientid

    def is_valid(self):
        # checks if any of the fields are empty
        if not self.username or not self.name or not self.email or not self.password  \
                or not self.phone or not self.room or not self.med or not self.patientid:
            return False
        else:
            return True
