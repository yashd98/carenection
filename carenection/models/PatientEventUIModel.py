from PyQt5.QtWidgets import *
from PyQt5.QtCore import *


class PatientEventUIModel(QWidget):
    def __init__(self, event_model):
        super(PatientEventUIModel, self).__init__()
        self.event_model = event_model
        self.checkbox = QCheckBox()
        self.outer_hbox_layout = QHBoxLayout()
        self.inner_hbox_layout = QHBoxLayout()
        self.inner_vbox_layout = QVBoxLayout()
        self.start_time = QLabel()
        self.end_time = QLabel()
        self.event_name = QLabel()
        self.line = QLabel()
        self.widget = self.createWidgets()

    def getWidget(self):
        return self.widget

    def createWidgets(self):
        self.start_time.setText(str(self.event_model.get_event_start().strftime("%I:%M %p")))
        self.end_time.setText(str(self.event_model.get_event_end().strftime("%I:%M %p")))
        self.event_name.setText("Event Name: " + self.event_model.get_event_name())

        self.start_time.setStyleSheet("color:white;font:14 pt;")
        self.end_time.setStyleSheet("color:white;font:14 pt;")
        self.event_name.setStyleSheet("color:white;font:14 pt;")

        self.line.setFixedSize(6, 70)
        self.line.setStyleSheet("background-color:red")

        self.inner_vbox_layout.addWidget(self.start_time)
        self.inner_vbox_layout.addWidget(self.end_time)

        self.inner_hbox_layout.addLayout(self.inner_vbox_layout)
        self.inner_hbox_layout.addWidget(self.line)
        self.inner_hbox_layout.addWidget(self.event_name)
        self.inner_hbox_layout.setStretch(2, 5)
        self.inner_hbox_layout.setStretch(3, 0)

        widget = QWidget()
        widget.setStyleSheet("QWidget{background-color:transparent;color:white;border-radius:30px; font-size: 20px}")

        self.inner_hbox_layout.addStretch()
        widget.setLayout(self.inner_hbox_layout)

        return widget


