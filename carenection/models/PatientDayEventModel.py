from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from models.PatientEventUIModel import PatientEventUIModel

class PatientDayEventModel(QWidget):
    def __init__(self, controller, date):
        super(PatientDayEventModel, self).__init__()
        self.controller = controller
        self.date = date
        self.vbox_layout = QVBoxLayout()
        self.dayDate = QLabel()
        self.inner_event_vbox = QVBoxLayout()
        self.widget = QWidget()
        self.events = []
        self.createWidgets()

    def getWidgets(self):
        return self.widget

    def createWidgets(self):
        self.dayDate.setText(self.date.strftime("%A, %b %d, %Y"))
        self.dayDate.setStyleSheet("color:white;font:40px;")
        self.updateEventModels()
        self.vbox_layout.addWidget(self.dayDate)
        self.vbox_layout.addLayout(self.inner_event_vbox)
        self.widget.setLayout(self.vbox_layout)


    def updateEventModels(self):

        self.events = self.controller.cal_adm_dt_sel_listener(self.date.date())

        if len(self.inner_event_vbox) > 0:
            for x in reversed(range(self.inner_event_vbox.count())):
                self.inner_event_vbox.takeAt(x).widget().setParent(None)

        # update the inner_event_vbox layout
        if len(self.inner_event_vbox) == 0 and len(self.events) > 0:
            for ev in self.events:
                self.inner_event_vbox.addWidget(PatientEventUIModel(ev).getWidget())