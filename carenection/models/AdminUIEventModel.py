from PyQt5.QtWidgets import *
from PyQt5.QtCore import *


class AdminUIEventModel(QWidget):
    def __init__(self, parent_class, event_model, event_list_counter):
        super(AdminUIEventModel, self).__init__()

        self.parent_class = parent_class
        self.event_model = event_model
        self.event_list_counter = event_list_counter
        self.checkbox = QCheckBox()
        self.outer_hbox_layout = QHBoxLayout()
        self.inner_hbox_layout = QHBoxLayout()
        self.inner_vbox_layout = QVBoxLayout()
        self.delete_btn = QPushButton()
        self.start_time = QLabel()
        self.end_time = QLabel()
        self.event_name = QLabel()
        self.line = QLabel()
        self.widget = self.createWidgets()
        self.delete_btn.clicked.connect(lambda: self.parent_class.delete_event(self.event_list_counter))


    def getWidget(self):
        return self.widget

    def createWidgets(self):
        self.start_time.setText(str(self.event_model.get_event_start().strftime("%I:%M %p")))
        self.end_time.setText(str(self.event_model.get_event_end().strftime("%I:%M %p")))
        self.event_name.setText("Event Name: " + self.event_model.get_event_name())

        self.delete_btn.setText(" X ")
        self.delete_btn.setStyleSheet(
            "QPushButton{background-color:black;color:white;border-radius:20px;}QPushButton:hover{"
            "background-color:white;color:black;}")
        self.delete_btn.setToolTip("Delete Event")

        self.line.setFixedSize(6, 70)
        self.line.setStyleSheet("background-color:#46CFEA")

        self.inner_vbox_layout.addWidget(self.start_time)
        self.inner_vbox_layout.addWidget(self.end_time)

        self.inner_hbox_layout.addLayout(self.inner_vbox_layout)
        self.inner_hbox_layout.addWidget(self.line)
        self.inner_hbox_layout.addWidget(self.event_name)
        self.inner_hbox_layout.addWidget(self.delete_btn)
        self.inner_hbox_layout.setStretch(2, 5)
        self.inner_hbox_layout.setStretch(3, 0)

        widget = QWidget()
        widget.setStyleSheet("QWidget{background-color:transparent;color:white;border-radius:30px; font-size: 35px;}")

        self.inner_hbox_layout.addStretch()
        widget.setLayout(self.inner_hbox_layout)

        return widget

    def updateIndex(self):
        self.event_list_counter -= 1
