import React from 'react';
import {FaBars} from 'react-icons/fa'



import { Nav, NavbarContainer, NavLogo, MobileIcon, NavMenu, NavItem, NavLinks, NavBtn, NavBtnLink } from './NavbarElements';


const Navbar= ({ toggle }) => {
  return (
    <>
      <Nav>
          <NavbarContainer>
              <NavLogo to='/'>Carenections</NavLogo>
              <MobileIcon onCLick= {toggle}>
                
              </MobileIcon>
              <NavMenu>
                  <NavItem>
                      <NavLinks to="about">About</NavLinks>
                  </NavItem>
                  <NavItem>
                      <NavLinks to="discover">Discover</NavLinks>
                  </NavItem>
                  <NavItem>
                      <NavLinks to="services">Services</NavLinks>
                  </NavItem>
                  <NavItem>
                      <NavLinks to="signup">Join Us</NavLinks>
                  </NavItem>
              </NavMenu>
              <NavBtn>
                  <NavBtnLink to='/signup'>Register</NavBtnLink> 
              </NavBtn>
              <NavBtn>
                  <NavBtnLink to='/signin'>Sign In</NavBtnLink> 
              </NavBtn>
          </NavbarContainer>
      </Nav>  
    </>
  )
}

export default Navbar; 

