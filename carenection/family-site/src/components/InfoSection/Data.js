import Grandma from '../../images/grandma.svg'
import Connected from '../../images/connected.svg'
import SignUp from '../../images/sign-up.svg'

export const homeObjOne = {
    id :'about',
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Personalized Experience',
    headline: 'Stay Connected and Build Memories',
    description: 'Talk, See, and be Updated about your loved ones, despite of the distance.',
    buttonLabel: 'Start Connecting',
    imgStart: false,
    img: Grandma,
    alt: 'Connections',
    dark: true,
    primary: true,
    darkText:false 
}

export const homeObjTwo = {
    id :'discover',
    lightBg: true,
    lightText: false,
    lightTextDesc: false,
    topLine: 'Video & Audio Calls',
    headline: 'Connect at anytime, you miss them!',
    description: 'Get access to our exclusive Audio and Video call service, with unlimited talk time.',
    buttonLabel: 'Learn More',
    imgStart: true,
    img: Connected,
    alt: 'Connections',
    dark: false,
    primary: false,
    darkText:true 
}

export const homeObjThree = {
    id :'signup',
    lightBg: true,
    lightText: false,
    lightTextDesc: false,
    topLine: 'Join Today',
    headline: 'Ready, Set, Go.',
    description: 'Creating an account is extremely easy. Get set up under 10 minutes.',
    buttonLabel: 'Start Now',
    imgStart: false,
    img: SignUp,
    alt: 'Connections',
    dark: false,
    primary: false,
    darkText:true 
}