import React from 'react'
import {FooterLinkItems, FooterLink, FooterContainer, FooterLinksContainer, FooterWrap, FooterLinksWrapper, FooterLinkTitle, SocialIconLink, SocialMediaWrap, SocialMediaWrapper, SocialIcons,SocialLogo, SocialMedia, WebsiteRights } from './FooterElements'
import {FaFacebook, FaInstagram, FaYoutube, FaTwitter, FaLinkedin} from 'react-icons/fa'


const Footer = () => {
  return (
    <FooterContainer>
      <FooterWrap>
          <FooterLinksContainer>
              <FooterLinksWrapper>
                  <FooterLinkItems>
                      <FooterLinkTitle> About Us </FooterLinkTitle>
                          <FooterLink to="/">How it works</FooterLink>
                          <FooterLink to="/">Testimonials</FooterLink>
                          <FooterLink to="/">Investors</FooterLink>
                          <FooterLink to="/">Terms of Service</FooterLink>          
                  </FooterLinkItems>
                  <FooterLinkItems>
                      <FooterLinkTitle> Contact Us </FooterLinkTitle>
                          <FooterLink to="/">Contact</FooterLink>
                          <FooterLink to="/">Support</FooterLink>
                          <FooterLink to="/">Sponsorships</FooterLink>
                  </FooterLinkItems>
              </FooterLinksWrapper>    
          </FooterLinksContainer>
         <SocialMedia>
             <SocialMediaWrap>
                <SocialLogo to='/'>
                   Carenections
                </SocialLogo>
                <WebsiteRights>Carenections © {new Date().getFullYear()} -
                  All rights reserved.</WebsiteRights>
                <SocialIcons>
                    <SocialIconLink href="//www.facebook.com" target="_blank" aria-label="Facebook"><FaFacebook /></SocialIconLink>
                    <SocialIconLink href="//www.instagram.com" target="_blank" aria-label="Instagram"><FaInstagram /></SocialIconLink>
                    <SocialIconLink href="//www.youtube.com" target="_blank" aria-label="Youtube"><FaYoutube /></SocialIconLink>
                    <SocialIconLink href="//www.twitter.com" target="_blank" aria-label="Twitter"><FaTwitter /></SocialIconLink>
                    <SocialIconLink href="//www.linkedin.com" target="_blank" aria-label="Linkedin"><FaLinkedin /></SocialIconLink>
                </SocialIcons>
             </SocialMediaWrap>
        </SocialMedia>
      </FooterWrap>
    </FooterContainer>
  )
}

export default Footer
