import React from 'react';
import Video from '../../videos/children.mp4';
import { Container, Icon, Form, FormH1, FormH2, Text, FormButton, FormContent, FormInput, FormLabel, FormWrap } from './SigninElements';
import { MainContainer, MainBg, VideoBg, MainContent, MainH1, MainP, MainBtnWrapper, ArrowForward, ArrowRight } from '../MainSection/MainElements';
const SignIn = () => {
    return (
        <>
        <Container>
            <FormWrap>
                <MainBg>
                    <VideoBg autoPlay loop muted src={Video} type='video/mp4' />
                </MainBg>
                
                
                <FormContent>
                    <Form action="#">
                        <FormH1>Welcome Back. Sign in here. </FormH1>
                        <FormLabel htmlFor='for'>Email</FormLabel>
                        <FormInput type='email' required />
                        <FormLabel htmlFor='for'>Password</FormLabel>
                        <FormInput type='password' required />
                        <FormButton type='submit'><FormH2>Sign In</FormH2></FormButton>
                        <br></br>
                        <FormButton> <Icon to="/">Back</Icon></FormButton>
                   </Form>
                </FormContent>
            </FormWrap>
        </Container>
        </>
    );
};

export default SignIn;

