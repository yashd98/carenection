import React from 'react'
import Icon1 from '../../images/connect.svg'
import Icon2 from '../../images/video_call_graphic.svg'
import Icon3 from '../../images/world.svg'
import { ServicesContainer, ServicesH1, ServicesH2, ServicesP, ServicesWrapper, ServicesCard, ServicesIcon} from './ServicesElements'

const index = () => {
  return (
    <>
      <ServicesContainer id ="services">
          <ServicesH1>Our Services</ServicesH1>
          <ServicesWrapper>
              <ServicesCard>
                  <ServicesIcon src={Icon1}/>
                  <ServicesH2>Check on Your Loved Ones</ServicesH2>
                  <ServicesP>Keep a check on how your loved ones. You can access our platform online anywhere in the world.</ServicesP>
              </ServicesCard>
              <ServicesCard>
                  <ServicesIcon src={Icon2}/>
                  <ServicesH2>Audio/Video Call Anytime</ServicesH2>
                  <ServicesP>We help connect families from near or far. You can call for as long as you want, using our secured Audio/Video calling feature. </ServicesP>
              </ServicesCard>
              <ServicesCard>
                  <ServicesIcon src={Icon3}/>
                  <ServicesH2>Add memories, while you make them</ServicesH2>
                  <ServicesP>Add photos to the picture frame, and send your message across.</ServicesP>
              </ServicesCard>
          </ServicesWrapper>
      </ServicesContainer>
    </>
  )
}

export default index
