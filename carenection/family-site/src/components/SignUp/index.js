import React from 'react';

import { Container, Icon, Form, FormH1, FormH2, Text, FormButton, FormContent, FormInput, FormLabel, FormWrap } from './SignupElements';


const SignUp = () => {
    return (
        <>
        <Container>
            <FormWrap>
                <Icon to="/">Carenections</Icon>
                    <FormContent>
                    <Form action="#">
                        <FormH1>Welcome. Register here. </FormH1>
                        <FormLabel htmlFor='for'>Username</FormLabel>
                        <FormInput type='text' required />
                        <FormLabel htmlFor='for'>Password</FormLabel>
                        <FormInput type='password' required />
                        <FormLabel htmlFor='for'>Full Name</FormLabel>
                        <FormInput type='text' required />
                        <FormLabel htmlFor='for'>Phone Number</FormLabel>
                        <FormInput type='number' required />
                        <FormLabel htmlFor='for'>Email</FormLabel>
                        <FormInput type='email' required />
                        <FormLabel htmlFor='for'>Patient ID</FormLabel>
                        <FormInput type='text' required />
                        <FormLabel htmlFor='for'>Relation</FormLabel>
                        <FormInput type='text' required />
                        <FormButton type='submit'><FormH2>Sign Up</FormH2></FormButton>
                    </Form>
                </FormContent>
            </FormWrap>
        </Container>
        </>
    );

};


export default SignUp;