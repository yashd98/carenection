import styled from "styled-components";
import { createGlobalStyle } from 'styled-components'
import Webcam from "react-webcam";
import Button from 'react-bootstrap/Button';

export const GlobalStyle = createGlobalStyle`
  body {
    background: #000;
  }
`
export const Container=styled.div`
`
export const PButton=styled(Button)`
margin-left: 44%;
 border-radius:50px;
        background: #46CFEA;
        white-space: nowrap;
        padding: 10px 22px;
        color: #010606;
        font-size: 16px;
        outline: none;
        border: none;cursor: pointer;
        transition: all 0.2s ease-in-out;
        text-decoration: none;

        &.hover { 

            transition: all 0.2s ease-in-out;
            background: #fff;
            color: #010606;
        }
`
export const PaitentWebcamStyled=styled(Webcam)`
position: absolute;
padding-top: 1%;
top: 0px;
right: 345px;
width: 200px;
height: 100px;
`

export const FamilyWebcamStyled=styled(Webcam)`
padding-top: 1%;
padding-left: 25%;
`