import React from 'react'
import InfoSection from '../components/SignUp'
import Video from '../videos/family.mp4'
import { Container, Icon, Form, FormH1, FormH2, Text, FormButton, FormContent, FormInput, FormLabel, FormWrap } from '../components/SignUp/SignupElements';
import { MainContainer, MainBg, VideoBg, MainContent, MainH1, MainP, MainBtnWrapper, ArrowForward, ArrowRight } from '../components/MainSection/MainElements';

const SignupPage = () => {
  return (
    <div>
       
       <Container>
      
            <FormWrap>
                
                <MainBg>
                <VideoBg autoPlay loop muted src={Video} type='video/mp4' />
            </MainBg>
                    <FormContent>
                    <Form action="#">
                    
                        <FormH1>Welcome. Register here. </FormH1>
                        <FormLabel htmlFor='for'>Username</FormLabel>
                        <FormInput type='text' required />
                        <FormLabel htmlFor='for'>Password</FormLabel>
                        <FormInput type='password' required />
                        <FormLabel htmlFor='for'>Full Name</FormLabel>
                        <FormInput type='text' required />
                        <FormLabel htmlFor='for'>Phone Number</FormLabel>
                        <FormInput type='number' required />
                        <FormLabel htmlFor='for'>Email</FormLabel>
                        <FormInput type='email' required />
                        <FormLabel htmlFor='for'>Patient ID</FormLabel>
                        <FormInput type='text' required />
                        <FormLabel htmlFor='for'>Relation</FormLabel>
                        <FormInput type='text' required />
                        <FormButton type='submit'><FormH2>Sign Up</FormH2></FormButton>
                        <br></br>
                        <FormButton> <Icon to="/">Back</Icon></FormButton>
                    </Form>
                </FormContent>
            </FormWrap>
        </Container>
    </div>
  )
}

export default SignupPage
