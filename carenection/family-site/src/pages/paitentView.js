import React, { useState } from 'react'
import{Container,PButton,PaitentWebcamStyled,FamilyWebcamStyled,GlobalStyle} from './paitentViewStyle/paitentView.styles'
const PaitentView = () => {
  const [callStatus,setCallStatus] = useState(true);
  const text=()=>{
    if (callStatus===true){
      return(
      <Container>
        <GlobalStyle/>    
          <PaitentWebcamStyled 
          audio={true} />
          <FamilyWebcamStyled/> 
        <PButton onClick={endCall}>
          End Call
        </PButton>
      </Container>);
    }
    else{
      return <div>Call Ended</div>;
    }
  }

  const endCall = () => {
    console.log(callStatus);
    setCallStatus('false')
    console.log(callStatus);

  }
  return (
    <div className='paitentView'>
      {text()}
    </div>
  )
}

export default PaitentView