import React, { useState } from 'react'
import{Container,EndButton,PaitentWebcamStyled,FamilyWebcamStyled,GlobalStyle,AudioButton,VidButton,BtnCont} from './familyViewStyle/familyView.styles'
const FamilyView = () => {
  const [callStatus,setCallStatus] = useState(true);
  const [audioStatus,setAudioStatus] = useState(true);
  const [audioBtn,setAudioBtn] = useState("Stop");
  const [vidBtn,setVidBtn] = useState("Stop");
  const [vidStatus,setVidStatus] = useState(true);
  const text=()=>{
    if (callStatus===true){
      return(
      <Container>
        <GlobalStyle/>    
          <PaitentWebcamStyled  />
          <FamilyWebcamStyled audio={audioStatus}/>
          <BtnCont>
            <EndButton onClick={endCall}>
              End Call
            </EndButton>
            <AudioButton onClick={flipAuido}>
              {audioBtn} Audio
            </AudioButton>
            <VidButton  onClick={flipVid}>
              {vidBtn} Video
            </VidButton>
          </BtnCont>
      </Container>);
    }
    else{
      return <div>Call Ended</div>;
    }
  }

  const endCall = () => {
    console.log(callStatus);
    setCallStatus('false')
    console.log(callStatus);

  }
  const flipAuido = () => {
    console.log(audioStatus);
    setAudioStatus(!audioStatus)
    console.log(audioStatus);
    if(audioStatus){
      setAudioBtn("Start")
    }
    else{
      setAudioBtn("Stop")
    }

  }
  const flipVid = () => {
    console.log(vidStatus);
    setVidStatus(!vidStatus)
    console.log(vidStatus);
    if(vidStatus){
      setVidBtn("Start")
    }
    else{
      setVidBtn("Stop")
    }

  }
  return (
    <div >
      {text()}
    </div>
  )
}

export default FamilyView