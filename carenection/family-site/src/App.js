import './App.css';

import {BrowserRouter as Router, Switch, Route  } from 'react-router-dom';
import SigninPage from './pages/signin';
import SignupPage from './pages/signup';
import FamilyView from './pages/familyView';
import PaitentView from './pages/paitentView';
import Home from './pages';
//<Route exact path="/familyView" component={FamilyView}/>
function App() {
  return (
    
    <Router>
      <Switch>
        
        
      <Route exact path="/signin" component={SigninPage}/>
      <Route exact path="/signup" component={SignupPage}/>
      <Route exact path="/familyView" component={FamilyView}/>
      <Route exact path="/paitentView" component={PaitentView}/>
      <Route exact path="/" component={Home}/>

      </Switch>
      
    </Router>
  );
}

export default App;
