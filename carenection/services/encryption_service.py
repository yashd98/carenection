from Crypto.Cipher import AES

from Crypto.Random import get_random_bytes


class AuthKeyEncoder():


    def __init__(self, auth_key, file_name):

        our_key = self.get_key()

        self.create_encrypted_file(auth_key, our_key, file_name)


    def create_encrypted_file(self, auth_key, unique_key, file_name):

        b_auth_key = bytes(auth_key, 'utf-8')

        cipher = AES.new(unique_key, AES.MODE_EAX)

        ciphertext, tag = cipher.encrypt_and_digest(b_auth_key)


        file_out = open(file_name, "wb")

        [file_out.write(x) for x in (cipher.nonce, tag, ciphertext)]

        self.encrypted_file = file_out

        file_out.close()


    def get_auth_key(self):

        our_key = self.get_key()

        file_in = open(self.encrypted_file.name, "rb")

        nonce, tag, ciphertext = [file_in.read(x) for x in (16, 16, -1)]

        cipher = AES.new(our_key, AES.MODE_EAX, nonce)

        b_auth_key = cipher.decrypt_and_verify(ciphertext, tag)


        return b_auth_key.decode("utf-8")


    def get_key(self):

        # return get_random_bytes(16)

        return b':\xad\xd3\x90\xaf\x1f\xb1\x10|\x8cw1|\x1f\xa8\xe6'


#

# if __name__ == "__main__":

#     auth_key_new = AuthKeyEncoder("1234324", "encrypted_file")

#     print(auth_key_new.encrypted_file.name)

#     print(auth_key_new.get_auth_key())