import backend_service

if __name__ == '__main__':

    for i in range(1, 10):
        username = "pt" + str(i)
        password = "pass"
        name = "Patient Name" + str(i)
        phone_number = 9057770000 + i
        email = "pt" + str(i) + "@yopmail.com"
        room_number = 3000 + i
        medical_condition = [[(i/10)], [i]]

        register_patient(username, password, name, phone_number, email, room_number, medical_condition)