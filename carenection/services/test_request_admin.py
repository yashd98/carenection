import backend_service
if __name__ == '__main__':

    for i in range(1, 5):
        username = "ad" + str(i)
        password = "pass"
        name = "Caregiver Name" + str(i)
        phone_number = 9055550000 + i
        email = "ad" + str(i) + "@yopmail.com"

        register_caregiver(username, password, name, phone_number, email)

    for i in range(1, 5):
        for k in range(1, 3):
            username = "p" + str(i) +"f" +str(k)
            password = "pass"
            name = "Patient" +str(i) +" Family" + str(k)
            phone_number = 9053330000 + i + k
            email = "p" + str(i) +"f" +str(k) + "@yopmail.com"
            patient_id = i
            relation = "Fam " +str(k)

            register_family(username, password, name, phone_number, email, patient_id, relation)