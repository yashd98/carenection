import requests
import json


class backend_service:
    # Events Services
    def add_event(self, authkey, pid, event, description, end_time, start_time):
        rbody = {
            "patient_id": pid,
            "event": event,
            "description": description,
            "end_time": end_time,
            "start_time": start_time,
            "authkey": authkey
        }

        return requests.post('https://carenections.herokuapp.com/add_event', json=rbody)

    def edit_event(self, authkey, pid, event, description, end_time, start_time):
        rbody = {
            "patient_id": pid,
            "event": event,
            "description": description,
            "end_time": end_time,
            "start_time": start_time,
            "authkey": authkey
        }

        return requests.post('https://carenections.herokuapp.com/edit-event', json=rbody)

    def remove_event(self, authkey, pid, end_time, start_time):
        rbody = {
            "patient_id": pid,
            "end_time": end_time,
            "start_time": start_time,
            "authkey": authkey
        }

        return requests.post('https://carenections.herokuapp.com/remove-event', json=rbody)

    def get_event(self, authkey,pid, start, end=None):
        rbody = {
            "patient_id": pid,
            "start": start,
            "authkey": authkey
        }

        if end != None:
            rbody['end'] = end

        return requests.get('https://carenections.heraokuapp.com/get-event', json=rbody)

    # Logbook Service
    def record_log(self, authkey,pid, adid, time, meal="", medicine=""):
        rbody = {
            "patient_id": pid,
            "admin_id": adid,
            "meal": meal,
            "medicine": medicine,
            "log_time": time,
            "authkey": authkey
        }

        return requests.post('https://carenections.herokuapp.com/record-log', json=rbody)

    def edit_log(self, authkey, pid, adid, time, meal="", medicine=""):
        rbody = {
            "patient_id": pid,
            "admin_id": adid,
            "meal": meal,
            "medicine": medicine,
            "log_time": time,
            "authkey": authkey
        }

        return requests.post('https://carenections.herokuapp.com/edit-log', json=rbody)

    def remove_log(self, authkey, pid, adid, time):
        rbody = {
            "patient_id": pid,
            "admin_id": adid,
            "log_time": time,
            "authkey": authkey
        }

        return requests.post('https://carenections.herokuapp.com/remove-log', json=rbody)

    def get_log(self, authkey, pid, start=None, end=None):
        rbody = {
            "patient_id": pid,
            "authkey": authkey
        }

        if start != None:
            rbody['start'] = start

        if end != None:
            rbody['end'] = end

        return requests.get('https://carenections.herokuapp.com/get-log', json=rbody)

    # Patient Login/Register
    def register_patient(self, authkey, username, password, name, phone_number, email, room_number, medical_condition):
        rbody = {
            "username": username,
            "password": password,
            "name": name,
            "phone_number": phone_number,
            "email": email,
            "room_number": room_number,
            "medical_condition": medical_condition,
            "authkey": authkey
        }

        return requests.post('https://carenections.herokuapp.com/register-patient', json=rbody)

    def login_patient(self, authkey, username, password):
        rbody = {
            "username": username,
            "password": password,
            "authkey": authkey
        }

        return requests.get('https://carenections.herokuapp.com/login-admin', json=rbody)

    # Admin Login/Register
    def register_caregiver(self, authkey, username, password, name, phone_number, email):
        rbody = {
            "username": username,
            "password": password,
            "name": name,
            "phone_number": phone_number,
            "email": email,
            "authkey": authkey
        }

        return requests.post('https://carenections.herokuapp.com/register-caregiver', json=rbody)

    def register_family(self, authkey, username, password, name, phone_number, email, pid, relation):
        rbody = {
            "username": username,
            "password": password,
            "name": name,
            "phone_number": phone_number,
            "email": email,
            "patient_id": pid,
            "relation": relation,
            "authkey": authkey
        }

        return requests.post('https://carenections.herokuapp.com/register-family', json=rbody)

    def login_admin(self, authkey, username, password):
        rbody = {
            "username": username,
            "password": password,
            "authkey": authkey
        }

        return requests.get('https://carenections.herokuapp.com/login-admin', json=rbody)

    # Contact Services
    def add_contact(self, authkey, username, pid, relation):
        rbody = {
            "username": username,
            "patient_id": pid,
            "relation": relation,
            "authkey": authkey
        }

        return requests.get('https://carenections.herokuapp.com/add-contact', json=rbody)

    def delete_contact(self, authkey, username, pid):
        rbody = {
            "username": username,
            "patient_id": pid,
            "authkey": authkey
        }

        return requests.get('https://carenections.herokuapp.com/delete_contact', json=rbody)
