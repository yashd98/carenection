from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5 import QtCore

from view.ContactSelectionPatientContainer import ContactSelectionPatientContainer


class VideoFrameContainer(QWidget):
    def __init__(self, controller):
        super(VideoFrameContainer, self).__init__()
        self.controller = controller
        self.stackedframes = QStackedWidget()
        self.contactsel = ContactSelectionPatientContainer(self.controller,self.stackedframes)
        self.stackedframes.setCurrentIndex(0)
        self.vbox = QVBoxLayout()
        self.widget = QWidget()

    def getVideoWidgets(self):
        self.vbox.addWidget(self.stackedframes)
        self.widget.setLayout(self.vbox)
        return self.widget

    def createVideoWidgets(self):
        pass
