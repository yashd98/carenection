from PyQt5.QtWidgets import *

from view.AdminLoginContainerView import AdminLoginContainerView
from view.AdminView import AdminView
from view.CalenderFrameContainer import CalenderFrameContainer
from view.LoginContainerView import LoginContainerView
from view.MainView import MainView
from PyQt5.QtGui import *
from view.PictureFrameView import PictureFrameView
from view.SetupUIContainer import SetupUIContainer
from view.VideoFrameContainer import VideoFrameContainer
from view.NotificationBoxVeiw import NotificationBoxFactory

class View(QMainWindow):
    def __init__(self, controller):
        super(View, self).__init__()
        # Keep reference to Controller,
        # other sub-controllers/methods can also be accessed through this reference
        self.controller = controller

        # ------UI Designing--------
        self.main_stacked_widget = QStackedWidget()

        # Initialize all view containers (this includes PictureUI, AdminUI, etc.)
        self.loginContainerView = LoginContainerView(self.controller)
        self.pictureFrame = PictureFrameView(self.controller)
        self.videoFrame = VideoFrameContainer(self.controller)
        self.calenderFrame = CalenderFrameContainer(self.controller)
        self.mainView = MainView(self.controller, self.pictureFrame, self.videoFrame, self.calenderFrame)
        self.adminview = AdminView(self.controller, self)
        self.adminlogincontainerview = AdminLoginContainerView(self.controller)
        self.notificationBoxFactory = NotificationBoxFactory()
        self.setupuicontainer = SetupUIContainer(self.controller, self)

        # Add all containers to main_stacked_widget
        self.main_stacked_widget.addWidget(self.loginContainerView.getLoginContainer())
        self.main_stacked_widget.addWidget(self.setupuicontainer.getWidgets())
        self.main_stacked_widget.addWidget(self.mainView.getMainView())
        self.main_stacked_widget.addWidget(self.adminlogincontainerview.getAdminLoginContainer())
        self.main_stacked_widget.addWidget(self.adminview.getAdminWidgets())
        # Set main_stacked_widget in central widget of QMainWindow
        self.setCentralWidget(self.main_stacked_widget)
