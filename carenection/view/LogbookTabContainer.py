from functools import partial

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import sys
import os

from models.LogbookModel import LogbookModel


class LogbookTabContainer(QWidget):
    def __init__(self, controller):

        super(LogbookTabContainer, self).__init__()
        self.main_lay = QVBoxLayout(self)
        self.main_lay.setContentsMargins(0, 0, 0, 0)
        self.empty_wid = QFrame()
        self.controller = controller
        self.removebtn = QPushButton()
        self.addbtn = QPushButton()
        # self.editbtn = QPushButton()
        self.logtable = QTableWidget()
        self.searchbar = QLineEdit()
        self.searchbtn = QPushButton()

        style = """
            QFrame{
                background-color: #212121;
            }
            QTableWidget{
                background-color: #212121;
                gridline-color: #111111;
                font-size: 20px;
            }
            QTableWidget::Item{
                background-color: white;
                color: #212121;
            }
            QHeaderView::section {
                padding: 4px;
                font-size: 20px;
                font-weight: bold;
            }
            QTableWidget::Item:selected{
                background-color: #42CFEA;
                border: 0px;
            }
            QPushButton{
                background-color: rgba(255,255,255,0);
            }
            QLineEdit#searchbar{
                background-color: white;
                color: black;
                border: 2px solid #212121;
                border-radius: 10px;
                padding: 0 8px;
                font-size: 20px;
            }
            QPushButton#searchiconbtn{
               /* background-color: #42CFEA; */
                border: 2px solid #212121;
                border-radius: 100px;
            }
            QMenu#popup{
                background-color: white;
                border: 2px solid #212121;
                font-size: 15px;
            }
            QMenu#popup::Item{
                background-color: transparent;
            }
            QMenu#popup::Item:selected{
                background-color: #42CFEA;
                color: #212121;
            }
        """
        self.empty_wid.setStyleSheet(style)
        self.create_widgets()

        self.main_lay.addWidget(self.empty_wid)

    def create_widgets(self):
        vlayout = QVBoxLayout()
        vlayout.setContentsMargins(0, 0, 0, 0)
        hlayout_actions = QHBoxLayout()
        hlayout_actions.setContentsMargins(5, 5, 5, 5)
        hlayout_search = QHBoxLayout()
        hlayout_search.setContentsMargins(5, 5, 1100, 0)
        hlayout_search.setSpacing(0)
        curdir = os.path.dirname(__file__)

        # searchbar and searchicon
        self.searchbar.setPlaceholderText("Search for any logs...")
        self.searchbar.setObjectName("searchbar")
        self.searchbar.setToolTip("Search for any logs")
        # self.searchbar.setFrame(True)
        height = self.searchbar.sizeHint().height()
        increase_percent = 0.80  # percentage to increase by (in decimals)
        self.searchbar.setFixedHeight(height * (1 + increase_percent))
        self.searchbar.textEdited.connect(self.textchanged) #TODO: LATER USE THIS TO PREDICT TEXT
        self.searchbar.keyPressEvent = self.keypress_listener

        scaled_pixmap_search = QPixmap(os.path.join(curdir, "icons/search.png"))#.scaled(self.searchbar.height()+20,
                                                                                 #       self.searchbar.height()+20,
                                                                                  #      Qt.AspectRatioMode.KeepAspectRatio)
        self.searchbtn.setIcon(QIcon(scaled_pixmap_search))
        icon_resize = QSize(height*(1+(increase_percent/2)),height*(1+(increase_percent/2)))
        self.searchbtn.setIconSize(icon_resize)
        self.searchbtn.setObjectName("searchiconbtn")
        self.searchbtn.setToolTip("Click here to search for specified logs")
        self.searchbtn.clicked.connect(self.search)
        hlayout_search.addWidget(self.searchbar)
        hlayout_search.addWidget(self.searchbtn)
        vlayout.addLayout(hlayout_search)

        # Table
        # Later add a handler to laod logs from database when this tab is selected

        columns = ["Patient ID", "Meal", "Medicine", "Administrator",
                   "Date & Time"]  # using '&&' to display '&' as its escape character
        self.logtable.setColumnCount(len(columns))
        self.logtable.setHorizontalHeaderLabels(columns)
        self.logtable.setSelectionBehavior(QAbstractItemView.SelectionBehavior.SelectRows)
        self.logtable.setEditTriggers(
            QAbstractItemView.EditTrigger.NoEditTriggers | QAbstractItemView.EditTrigger.DoubleClicked)
        header = self.logtable.horizontalHeader()
        # header.setSectionResizeMode(0, QHeaderView.ResizeMode.ResizeToContents)
        header.setSectionResizeMode(0, QHeaderView.ResizeMode.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.ResizeMode.Stretch)
        header.setSectionResizeMode(2, QHeaderView.ResizeMode.Stretch)
        header.setSectionResizeMode(3, QHeaderView.ResizeMode.ResizeToContents)
        header.setSectionResizeMode(4, QHeaderView.ResizeMode.ResizeToContents)
        # set custompopupmenu policy and connect custommenu popup
        self.logtable.setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu)
        self.logtable.customContextMenuRequested.connect(self.menu_popup)

        vlayout.addWidget(self.logtable)

        # Add
        pmap_add = QPixmap(os.path.join(curdir, "icons/add_1.png"))
        self.addbtn.setIcon(QIcon(pmap_add))
        self.addbtn.setFixedSize(100, 100)
        self.addbtn.setIconSize(QSize(self.addbtn.height(), self.addbtn.height()))
        self.addbtn.setToolTip("Click here to add rows for logs")
        self.addbtn.clicked.connect(lambda: self.logbook_add_clicked(0))

        # Remove
        pmap_rem = QPixmap(os.path.join(curdir, "icons/remove_1.png"))
        self.removebtn.setIcon(QIcon(pmap_rem))
        self.removebtn.setFixedSize(100, 100)
        self.removebtn.setIconSize(QSize(self.addbtn.height(), self.addbtn.height()))
        self.removebtn.setToolTip("Click here to remove selected rows containing logs")
        self.removebtn.clicked.connect(self.logbook_del_clicked)

        hlayout_actions.addWidget(self.addbtn)
        hlayout_actions.addWidget(self.removebtn)
        # hlayout_actions.addWidget(self.editbtn)
        vlayout.addLayout(hlayout_actions)

        self.empty_wid.setLayout(vlayout)

    # local listeners
    # local listeners makes call to Controller's listeners
    def menu_popup(self, position):
        globalpos = self.logtable.mapToGlobal(position)
        modelindex = self.logtable.indexAt(position)
        # row and col starts from 0
        row = modelindex.row()
        col = modelindex.column()
        # print("Row: ", row, " Col: ", col)

        menu = QMenu(self.logtable)
        menu.setToolTipsVisible(True)
        menu.setObjectName("popup")
        insertabove = QAction("Insert Above")
        insertabove.setToolTip("Insert a row above the current row")
        insert = QAction("Insert")
        insert.setToolTip("Insert a row at the end of existing rows")
        insertbelow = QAction("Insert Below")
        insertbelow.setToolTip("Insert a row below the current row")
        edit = QAction("Edit")
        edit.setToolTip("Edit the current cell")

        # triggers/click events
        insertabove.triggered.connect(lambda: self.logbook_add_clicked(-1))
        insert.triggered.connect(lambda: self.logbook_add_clicked(0))
        insertbelow.triggered.connect(lambda: self.logbook_add_clicked(1))
        edit.triggered.connect(lambda: self.logtable.edit(modelindex))  # mimic double click at given row and col

        # print("Global Pos - ", self.logtable.mapToGlobal(position), " Local Pos - ", position, " Index - ", self.logtable.indexAt(position).column())

        menu.addAction(insertabove)
        menu.addSeparator()
        menu.addAction(insert)
        menu.addSeparator()
        menu.addAction(insertbelow)
        menu.addSeparator()
        menu.addAction(edit)
        menu.exec_(globalpos)
        menu.setStyleSheet("background-color:black")

    # LATER USE EVENT FILTER TO CAPTURE FOCUS IN AND FOCUS OUT AND UPDATE THE ENTRIES(DB AND TABLE) BASED ON THAT

    def logbook_add_clicked(self,
                            relpos):  # relative position to 0, -1 - insertabove, 0 - insertcurrent, 1 - insertbelow
        # call to controller
        rowpos = self.logtable.rowCount()  # insert
        if relpos == -1:  # insertabove
            rowpos = (self.getSelRows()[0]).row() - 1
            rowpos = 0 if rowpos < 0 else rowpos  # check to make sure insertion row loc is valid
        elif relpos == 1:  # insertbelow
            rowpos = (self.getSelRows()[0]).row() + 1

        self.logtable.insertRow(rowpos)
        # print("row inserted-", rowpos)

        self.logtable.setItem(rowpos, 0, QTableWidgetItem())
        self.logtable.setItem(rowpos, 1, QTableWidgetItem())
        self.logtable.setItem(rowpos, 2, QTableWidgetItem())
        self.logtable.setItem(rowpos, 3, QTableWidgetItem())
        self.logtable.setItem(rowpos, 4, QTableWidgetItem())

    def logbook_del_clicked(self):
        # call to controller
        rows = self.getSelRows()
        indices = []
        for index in rows:
            indices.append(index.row())

        indices = sorted(indices, reverse=True)

        for rowInd in indices:
            self.logtable.removeRow(rowInd)

    def getSelRows(self):
        return self.logtable.selectionModel().selectedRows()

    def reload_contents(self):
        pass

    def search(self):
        pass

    def textchanged(self):
        pass

    def keypress_listener(self,event):
        isenterkey = self.controller.keypress_listener(event)
        if not isenterkey:
            QLineEdit.keyPressEvent(self.searchbar,event)