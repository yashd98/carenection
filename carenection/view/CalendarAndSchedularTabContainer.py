from PyQt5.QtWidgets import *

from view.CalendarAdminContainer import CalendarAdminContainer


class CalendarAndSchedularTabContainer(QWidget):
    def __init__(self, controller, view):
        super(CalendarAndSchedularTabContainer, self).__init__()
        self.controller = controller
        self.view = view
        self.calendar_stacked_widget = QStackedWidget()
        self.calendar_admin_container = CalendarAdminContainer(self.controller,self.calendar_stacked_widget, self.view)
        self.calendar_stacked_widget.setCurrentIndex(0)
        self.vbox = QVBoxLayout()
        self.widget = QWidget()
        self.widget.setStyleSheet("background-color:#212121;")

    def getWidget(self):
        self.vbox.addWidget(self.calendar_stacked_widget)
        self.widget.setLayout(self.vbox)
        return self.widget