import PyQt5
from PyQt5.QtWidgets import *
from PyQt5 import QtCore
from PyQt5.QtGui import *
from pathlib import Path
import os

class AdminLoginContainerView(QMainWindow):
    def __init__(self, controller):
        super(AdminLoginContainerView, self).__init__()

        self.controller = controller
        # The final widget that is returned
        self.main_cont = QWidget()
        # The main screen AKA Admin Login Screen
        self.admin_login_screen = QHBoxLayout()
        # Left and Right sections of the screen
        self.left_section = QVBoxLayout()
        self.right_section = QWidget()
        #Login Label Space
        self.label = QWidget()


        # LEFT SECTION: The icon on the left Side
        self.admin_login_page_icon = QLabel()
        # RIGHT SECTION: Label, username, password, login button
        self.admin_login_label = QLabel()
        self.admin_login_label.setText("Admin Log In")
        self.admin_login_label.setObjectName("loginTitle")
        self.admin_username = QLineEdit()
        self.admin_username.setObjectName("uname")
        self.admin_pswrd = QLineEdit()
        self.admin_pswrd.setObjectName("pwd")
        self.admin_login_btn = QPushButton("LOGIN")
        self.admin_login_btn.setObjectName('loginBtn')
        # A Vertical layout to put the username, password and button together in
        self.user_pswrd_btn_layout = QVBoxLayout()
        self.login_btn_layout = QVBoxLayout()
        self.container_layout = QVBoxLayout()

        #Returned
        self.admin_login_container = self.createLoginWidgets()
        self.setActionListener()

    def getAdminLoginContainer(self):
        return self.admin_login_container

    def setActionListener(self):
        self.admin_login_btn.clicked.connect(lambda: self.controller.admin_login_action_listener(self.admin_username.text(), self.admin_pswrd.text()))

    def createLoginWidgets(self):

        #LEFT SECTION
        # Shield Icon
        file_dir = os.path.dirname(os.path.realpath(__file__))
        admin_login_page_icon_path = os.path.join(file_dir, "icons/shield.png")
        self.admin_login_page_icon.setPixmap(QPixmap(admin_login_page_icon_path))
        self.admin_login_page_icon.setGeometry(QtCore.QRect(65, 250, 281, 281))

        # Adding the Icon to Left Section
        self.left_section.addWidget(self.admin_login_page_icon)

        #RIGHT SECTION
        self.admin_login_label.setStyleSheet("#loginTitle{font-weight: bold; font-size: 75px; color: #282828;}")
        self.admin_username.setPlaceholderText("Admin User Name")
        self.admin_pswrd.setPlaceholderText("Admin Password")
        #css for the labels
        self.admin_username.setStyleSheet("#uname{background-color: rgba(0,0,0,0); border:none; border-bottom:3px solid#46CFEA; color:rgba(0,0,0,240); font-size:50px; }")
        self.admin_pswrd.setStyleSheet("#pwd{background-color: rgba(0,0,0,0); border:none; border-bottom:3px solid#46CFEA; color:rgba(0,0,0,240); font-size:50px; }")

        # Make the password hidden
        self.admin_pswrd.setEchoMode(QLineEdit.Password)

        #BUTTON
        self.admin_login_btn.setStyleSheet("#loginBtn{background-color: qconicalgradient(cx:0.537, cy:1, angle:358.1, stop:0 rgba(89, 190, 149, 255), stop:1 rgba(255, 255, 255, 255)); font-size:40px; height:70px; border-radius: 12px;}")

        #Add Username and Password in 1 Vertical layout
        self.user_pswrd_btn_layout.addWidget(self.admin_username)
        self.user_pswrd_btn_layout.addWidget(self.admin_pswrd)

        # Login Button Label in 1 layout
        self.login_btn_layout.addWidget(self.admin_login_btn)
        self.login_btn_layout.setContentsMargins(0, 30, 0, 70)

        #Login Label, username/password and login button in 1 layout
        self.container_layout.addWidget(self.admin_login_label)
        self.admin_login_label.setContentsMargins(0, 40, 0, 0)
        self.container_layout.addLayout(self.user_pswrd_btn_layout)
        self.user_pswrd_btn_layout.setSpacing(60)
        self.user_pswrd_btn_layout.setContentsMargins(0, 10, 0, 100)
        self.container_layout.addLayout(self.login_btn_layout)
        self.container_layout.setSpacing(10)

        # Add the above to the right section
        self.right_section.setLayout(self.container_layout)
        self.right_section.setStyleSheet("Background-color: white; color:black; border-radius:50px;}")
        self.right_section.setContentsMargins(100,100,200,140)
        self.left_section.setContentsMargins(250, 100, 250, 100)

        # Add both sections to the main screen
        self.admin_login_screen.addLayout(self.left_section)
        self.admin_login_screen.addWidget(self.right_section)

        #Styling Main Container
        self.main_cont.setStyleSheet("background-color:#282828;")

        # Adding Main Screen to Main Container
        self.main_cont.setLayout(self.admin_login_screen)

        return self.main_cont

