import os

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5 import QtCore

from models.ContactModel import ContactModel
from models.CustomContactListItem import CustomContactListItem
from models.CustomDisplayContactLabel import CustomDisplayContactLabel
from view.AudioVideoSelection import AudioVideoSelection


class ContactSelectionPatientContainer(QWidget):
    def __init__(self, controller, stackedframes):
        super(ContactSelectionPatientContainer, self).__init__()
        self.controller = controller
        self.main_lay = QVBoxLayout(self)
        self.main_lay.setContentsMargins(0, 0, 0, 0)
        self.empty_wid = QFrame()
        # self.addbtn = QPushButton()
        self.contacts_list = QListWidget()
        self.display_contact = CustomDisplayContactLabel()
        self.stackedframes = stackedframes
        self.audiovideosel = AudioVideoSelection(self.stackedframes)

        style = """
                    QFrame{
                        background-color: #212121; 
                    }
                    QFrame#separatorline{
                        background-color: white;

                    }
                    QListWidget{
                        background-color: #212121;
                    }
                    QPushButton{
                        background-color: #46CFEA;
                        color: #212121;
                        font-size: 20px;
                        border: 2px solid #212121;
                        border-radius: 16px;
                    }
                    QPushButton:hover{
                        border: 2px solid white;
                    }
                    QPushButton:focus{
                        outline: none; /* removes extra dotted rectangle */
                    }

                """
        self.empty_wid.setStyleSheet(style)
        self.createVideoWidgets()
        self.main_lay.addWidget(self.empty_wid)

    def getVideoWidgets(self):
        return self.empty_wid

    def createVideoWidgets(self):
        hlayout = QHBoxLayout()
        hlayout.setContentsMargins(0, 0, 0, 0)
        vlayout = QVBoxLayout()
        vlayout.setContentsMargins(5, 10, 5, 5)

        # curdir = os.path.dirname(os.getcwd())
        # pmap_add = QPixmap(os.path.join(curdir, "view/icons/add_2.png"))
        # resize_height = self.addbtn.sizeHint().height() * (1 + 0.50)
        # self.addbtn.setFixedSize(resize_height, resize_height)
        # self.addbtn.setIcon(QIcon(pmap_add))
        # self.addbtn.setToolTip("Click here to add a new contact")
        # self.addbtn.clicked.connect(lambda: self.dialog_popup(1))
        # vlayout.addWidget(self.addbtn, alignment=Qt.AlignRight)

        # self.contacts_list.set
        self.contacts_list.itemClicked.connect(self.contactSelected)
        self.contacts_list.setToolTip("All contacts for the current user/patient")
        vlayout.addWidget(self.contacts_list)
        hlayout.addLayout(vlayout)

        vline_separator = QFrame()
        vline_separator.setGeometry(320, 150, 118, 3)
        vline_separator.setObjectName("separatorline")
        vline_separator.setFrameShape(QFrame.VLine)
        vline_separator.setFrameShadow(QFrame.Sunken)
        hlayout.addWidget(vline_separator)

        # self.display_contact.hide() # dont show anything at the start
        self.display_contact.hide_widgets()
        # self.display_contact.show_widgets()
        self.display_contact.setObjectName("customlabel")
        self.display_contact.setToolTip("Selected contact information is displayed here")
        hlayout.addWidget(self.display_contact)

        self.empty_wid.setLayout(hlayout)

        # temp
        self.addToList()

        # Stacked widget
        self.stackedframes.addWidget(self)
        self.stackedframes.addWidget(self.audiovideosel.getWidgets())

    def contactSelected(self):
        data = self.getSelectedContact().model.getData()
        self.display_contact.setData(data)
        self.display_contact.show_widgets()

        self.audiovideosel.updateInfo(data)
        self.stackedframes.setCurrentIndex(1)
        print("Updated")

    def getSelectedContact(self):
        sel_row = self.contacts_list.selectionModel().selectedRows()[0]
        item = self.contacts_list.item(sel_row.row())
        # item_widget = self.contacts_list.itemWidget(item)
        # print("sel_row: ", sel_row.row(), " item: ", item, " item widget: ", item_widget)
        return self.contacts_list.itemWidget(item)

    def addToList(self):
        # print(model)
        # call to backend to add new contact to the db
        citem = CustomContactListItem(ContactModel("Kamal Patel", "kamalpatel@gmail.com", "Friend"), 0)
        item = QListWidgetItem(self.contacts_list)  # , self.contacts_list)
        item.setSizeHint(citem.sizeHint())
        self.contacts_list.addItem(item)
        self.contacts_list.setItemWidget(item, citem)

        citem = CustomContactListItem(ContactModel("Kamal Patel1", "kamalpatel1@gmail.com", "Friend1"), 0)
        item = QListWidgetItem(self.contacts_list)  # , self.contacts_list)
        item.setSizeHint(citem.sizeHint())
        self.contacts_list.addItem(item)
        self.contacts_list.setItemWidget(item, citem)

        citem = CustomContactListItem(ContactModel("Kamal Patel2", "kamalpatel2@gmail.com", "Friend2"), 0)
        item = QListWidgetItem(self.contacts_list)  # , self.contacts_list)
        item.setSizeHint(citem.sizeHint())
        self.contacts_list.addItem(item)
        self.contacts_list.setItemWidget(item, citem)
