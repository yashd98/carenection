from PyQt5.QtGui import QIcon, QPixmap, QImage
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt
import os


class ErrorNotificationDialog(QDialog):

    def __init__(self, window):
        QDialog.__init__(self, window)

    def close_clicked(self):
        self.close()


class SucessNotificationDialog(QDialog):

    def __init__(self, window):
        QDialog.__init__(self, window)

    def close_clicked(self):
        self.close()


class NotificationBoxFactory:

    def __init__(self):
        pass

    def createGenericNotificationBox(self, window, message):
        # specifying notification box specifications
        ui_notification_box = QDialog(window)
        ui_notification_box.setStyleSheet("QWidget{background-color:gray}")
        ui_notification_box.setFixedSize(1000, 400)
        ui_notification_box.setWindowTitle("Notification")

        ui_notification_box_v_layout = QVBoxLayout()
        ui_notification_box.setLayout(ui_notification_box_v_layout)

        # adding message to v_layout
        message_container = QLabel(message)
        message_container.setStyleSheet(
            "QLabel{color:black;font:50px 'Helvetica';qproperty-alignment: AlignCenter; font-weight: bold}")
        ui_notification_box_v_layout.addWidget(message_container)

        return ui_notification_box

    def createErrorNotification(self, window, message):
        # specifying notification box specifications
        error_msg_box = ErrorNotificationDialog(window)
        error_msg_box.setWindowTitle('Error!')
        error_msg_box_box_v_layout = QVBoxLayout()
        error_msg_box.setLayout(error_msg_box_box_v_layout)

        file_dir = os.path.dirname(os.path.realpath(__file__))
        file_name = os.path.join(file_dir, "icons/error-icon.png")
        err_icon = QPixmap(file_name)
        err_icon = err_icon.scaled(50, 50)
        icon_label = QLabel()
        icon_label.setPixmap(err_icon)

        message_label = QLabel(message)
        message_label.setWordWrap(True)
        message_label.setStyleSheet(
            "QLabel {qproperty-alignment: AlignCenterLeft; padding: 10 px; margin: 15 px; "
            "border: 0px} "
        )

        icon_label.setStyleSheet(
            "QLabel {qproperty-alignment: AlignCenter; padding: 0 px; margin: 0px; border: 0px}"
        )

        error_msg_box_box_h_layout = QHBoxLayout()
        error_msg_box_box_h_layout.addWidget(icon_label)
        error_msg_box_box_h_layout.addWidget(message_label)

        error_msg_box_box_v_layout.addLayout(error_msg_box_box_h_layout)
        accept_btn = QPushButton('Ok')
        accept_btn.clicked.connect(error_msg_box.close_clicked)
        error_msg_box_box_v_layout.addWidget(accept_btn)

        return error_msg_box

    def createSuccessNotification(self, window, message):
        # specifying notification box specifications
        success_msg_box = SucessNotificationDialog(window)
        success_msg_box.setWindowTitle('Success!')
        success_msg_box_v_layout = QVBoxLayout()
        success_msg_box.setLayout(success_msg_box_v_layout)

        file_dir = os.path.dirname(os.path.realpath(__file__))
        file_name = os.path.join(file_dir, "icons/check-icon.png")
        err_icon = QPixmap(file_name)
        err_icon = err_icon.scaled(50, 50)
        icon_label = QLabel()
        icon_label.setPixmap(err_icon)

        message_label = QLabel(message)
        message_label.setWordWrap(True)
        message_label.setStyleSheet(
            "QLabel {qproperty-alignment: AlignCenterLeft; padding: 10 px; margin: 15 px; "
            "border: 0px} "
        )

        icon_label.setStyleSheet(
            "QLabel {qproperty-alignment: AlignCenter; padding: 0 px; margin: 0px; border: 0px}"
        )

        success_msg_box_h_layout = QHBoxLayout()
        success_msg_box_h_layout.addWidget(icon_label)
        success_msg_box_h_layout.addWidget(message_label)

        success_msg_box_v_layout.addLayout(success_msg_box_h_layout)
        accept_btn = QPushButton('Ok')
        accept_btn.clicked.connect(success_msg_box.close_clicked)
        success_msg_box_v_layout.addWidget(accept_btn)

        return success_msg_box
