from PyQt5.QtWidgets import *
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5 import QtCore
from PyQt5 import QAxContainer
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QVBoxLayout, QPushButton
from PyQt5.QtWidgets import QWidget
from pathlib import Path


class InstrManualTabContainer(QWidget):
    def __init__(self):
        self.layout = QHBoxLayout()
        super(InstrManualTabContainer, self).__init__()
        self.mainScreen = QFrame()
        self.mainScreen.setObjectName("mainScreen")
        # 1. Menu and 2. Content Display
        self.instrScreen = QHBoxLayout()

        # Left
        self.menu = QWidget()
        self.menu.setObjectName("menu")
        self.buttonLayout = QVBoxLayout()
        self.topic = QPushButton("TOPICS")
        self.topic.setObjectName("topic")
        self.menu.setFixedHeight(723)
        self.menu.setGeometry(0, 0, 200, 70)
        self.message= QLabel("This is a guide to the different features of this Application. Please select the button on the left to open the PDF with the user guide for that section. PLease allow few seconds for the PDF to load.")
        self.message.setObjectName("msg")
        self.spacer = QLabel()
        self.spacer.setObjectName("spacer")

        # Right
        self.contentDisplay = QLabel()
        self.contentDisplay.setObjectName("display")

        # PDF Viewer
        self.WebBrowser = QAxContainer.QAxWidget(self.contentDisplay)
        self.WebBrowser.setFocusPolicy(Qt.StrongFocus)
        self.WebBrowser.setControl("{8856F961-340A-11D0-A96B-00C04FD705A2}")

        self.WebBrowser.adjustSize()
        self.WebBrowser.setGeometry(QtCore.QRect(-1, -10, 1200, 1355))
        self.WebBrowser.resize(self.WebBrowser.width(), self.WebBrowser.height())
        self.contentDisplay.setScaledContents(True)

        # Buttons
        self.patientLogin = QPushButton("Patient Login")
        self.patientLogin.setObjectName("plBtn")
        self.adminLogin = QPushButton("Admin Login")
        self.adminLogin.setObjectName("adBtn")
        self.contacts = QPushButton("Contacts")
        self.contacts.setObjectName("conBtn")
        self.logbook = QPushButton("Log Book")
        self.logbook.setObjectName("lbBtn")
        self.picFrame = QPushButton("Picture Frame")
        self.picFrame.setObjectName("pfBtn")
        self.calAndScheduler = QPushButton("Calendar && Scheduler")
        self.calAndScheduler.setObjectName("schBtn")
        self.help = QPushButton("Help Button for Patients")
        self.help.setObjectName("help")
        self.patientLogin.clicked.connect(self.plBtn_action)
        self.adminLogin.clicked.connect(self.adBtn_action)
        self.contacts.clicked.connect(self.conBtn_action)
        self.logbook.clicked.connect(self.lbBtn_action)
        self.picFrame.clicked.connect(self.pfBtn_action)
        self.calAndScheduler.clicked.connect(self.schBtn_action)
        self.help.clicked.connect(self.help_action)

        self.instr_man_container = self.createInstrManWidgets()
        self.pl_Btn_pdf_uri = str(Path(__file__).parent / "pdf/patientLogin.pdf")
        self.adm_Btn_pdf_uri = str(Path(__file__).parent / "pdf/adminLogin.pdf")
        self.help_pdf_uri = str(Path(__file__).parent / "pdf/help.pdf")
        self.logbook_pdf_uri = str(Path(__file__).parent / "pdf/logbook.pdf")
        self.pic_pdf_uri = str(Path(__file__).parent / "pdf/picFrame.pdf")
        self.sch_pdf_uri = str(Path(__file__).parent / "pdf/scheduler.pdf")
        self.settings_pdf_uri = str(Path(__file__).parent / "pdf/settings.pdf")
        self.cont_pdf_uri = str(Path(__file__).parent / "pdf/contacts.pdf")

    def plBtn_action(self):
        # convert system path to web path
        f = Path(self.pl_Btn_pdf_uri).as_uri()
        # load object
        self.WebBrowser.dynamicCall('Navigate(const QString&)', f)

    def help_action(self):
        # convert system path to web path
        f = Path(self.help_pdf_uri).as_uri()
        # load object
        self.WebBrowser.dynamicCall('Navigate(const QString&)', f)

    def adBtn_action(self):
        # convert system path to web path
        f = Path(self.adm_Btn_pdf_uri).as_uri()
        # load object
        self.WebBrowser.dynamicCall('Navigate(const QString&)', f)

    def conBtn_action(self):
        # convert system path to web path
        f = Path(self.cont_pdf_uri).as_uri()
        # load object
        self.WebBrowser.dynamicCall('Navigate(const QString&)', f)

    def pfBtn_action(self):
        # convert system path to web path
        f = Path(self.pic_pdf_uri).as_uri()
        # load object
        self.WebBrowser.dynamicCall('Navigate(const QString&)', f)

    def lbBtn_action(self):
        # convert system path to web path
        f = Path(self.logbook_pdf_uri).as_uri()
        # load object
        self.WebBrowser.dynamicCall('Navigate(const QString&)', f)

    def schBtn_action(self):
        # convert system path to web path
        f = Path(self.sch_pdf_uri).as_uri()
        # load object
        self.WebBrowser.dynamicCall('Navigate(const QString&)', f)

    def getInstrManWidgets(self):
        return self.instr_man_container

    def createInstrManWidgets(self):

        # Button Stylesheets
        self.patientLogin.setStyleSheet(
            "#plBtn{background-color:#46cfea; font-color:black; border-radius: 20px; font-size: 30px; }")
        self.adminLogin.setStyleSheet(
            "#adBtn{background-color:#46cfea; font-color:black; border-radius: 20px; font-size: 30px; }")
        self.contacts.setStyleSheet(
            "#conBtn{background-color:#46cfea; font-color:black; border-radius: 20px; font-size: 30px; }")
        self.logbook.setStyleSheet(
            "#lbBtn{background-color:#46cfea; font-color:black; border-radius: 20px; font-size: 30px; }")
        self.picFrame.setStyleSheet(
            "#pfBtn{background-color:#46cfea; font-color:black; border-radius: 20px; font-size: 30px; }")
        self.help.setStyleSheet(
            "#help{background-color:#46cfea; font-color:black; border-radius: 20px; font-size: 30px; }")
        self.calAndScheduler.setStyleSheet(
            "#schBtn{background-color:#46cfea;font-color:black; border-radius: 20px; font-size: 30px;}")
        self.menu.setStyleSheet("#menu{background-color:#212121; border-radius:50px; font-weight:bold}")
        self.contentDisplay.setStyleSheet("#display{background-color:#46cfea; border-radius:50px; border: 3px solid black}")
        self.topic.setStyleSheet("#topic{color: white; background-color: #212121;font-size: 30px;}")
        self.mainScreen.setStyleSheet("#mainScreen{background-color: #212121; border: 0px}")
        self.spacer.setStyleSheet("#spacer{color:white;}")


        # Topic Menu
        self.menu.setContentsMargins(30, 20, 50, 0)
        self.menu.setFixedWidth(500)
        # PDF Display
        self.contentDisplay.setContentsMargins(400, 100, 200, 140)
        self.contentDisplay.setFixedWidth(1200)
        # self.calAndScheduler.setFixedWidth(300)
        # Vertical Layout to put Buttons
        self.buttonLayout.addWidget(self.spacer)
        self.buttonLayout.addWidget(self.spacer)
        self.buttonLayout.addWidget(self.topic)
        self.buttonLayout.addWidget(self.patientLogin)
        self.buttonLayout.addWidget(self.adminLogin)
        self.buttonLayout.addWidget(self.contacts)
        self.buttonLayout.addWidget(self.logbook)
        self.buttonLayout.addWidget(self.picFrame)
        self.buttonLayout.addWidget(self.calAndScheduler)
        self.buttonLayout.addWidget(self.help)
        self.buttonLayout.addWidget(self.spacer)
        self.buttonLayout.addWidget(self.spacer)

        self.menu.setLayout(self.buttonLayout)

        self.instrScreen.addWidget(self.menu)
        self.instrScreen.addWidget(self.contentDisplay)
        self.mainScreen.setLayout(self.instrScreen)
        self.layout.addWidget(self.mainScreen)

        self.setLayout(self.layout)

