from PyQt5.QtWidgets import *
from PyQt5 import QtCore
from PyQt5.QtGui import *
class SetupUIContainer(QWidget):
    def __init__(self, controller, view):
        super(SetupUIContainer, self).__init__()
        self.controller = controller
        self.view = view
        self.vbox = QVBoxLayout()
        self.frame_vbox = QVBoxLayout()
        self.user_hbox = QHBoxLayout()
        self.pass_hbox = QHBoxLayout()
        self.email_hbox = QHBoxLayout()
        self.phone_hbox = QHBoxLayout()
        self.name_hbox = QHBoxLayout()
        self.room_hbox = QHBoxLayout()
        self.medicalCond_hbox = QHBoxLayout()
        self.dob_hbox = QHBoxLayout()
        self.gender_hbox = QHBoxLayout()
        self.accType_hbox = QHBoxLayout()
        self.title = QLabel()
        self.panel = QWidget()
        self.input_var_vbox = QVBoxLayout()
        self.name_lbl = QLabel()
        self.name = QLineEdit()
        self.username_lbl = QLabel()
        self.username = QLineEdit()
        self.phone_lbl = QLabel()
        self.phone = QLineEdit()
        self.password_lbl = QLabel()
        self.password = QLineEdit()
        self.password.setEchoMode(QLineEdit.Password)
        self.email_lbl = QLabel()
        self.email = QLineEdit()
        self.room_lbl = QLabel()
        self.room = QLineEdit()
        self.medicalCond_lbl = QLabel()
        self.medicalCond = QLineEdit()
        self.gender_lbl = QLabel()
        self.gender = QLineEdit()
        self.male = QRadioButton()
        self.female = QRadioButton()
        self.other = QRadioButton()
        self.noAns = QRadioButton()
        self.accType_lbl = QLabel()
        self.accType = QLineEdit()
        self.family = QRadioButton()
        self.admin = QRadioButton()
        self.patient = QRadioButton()
        self.dob_lbl = QLabel()
        self.dob = QLineEdit()
        self.btn_vbox = QVBoxLayout()
        self.finish = QPushButton()
        self.back = QPushButton()
        self.widget = self.createWidgets()
        self.set_action_listener()

    def back_action_listener(self):
        self.view.main_stacked_widget.setCurrentIndex(0)

    def finish_action_listener(self):
        self.view.main_stacked_widget.setCurrentIndex(2)

    def set_action_listener(self):
        self.back.clicked.connect(self.view.controller.backActionListener)
        self.finish.clicked.connect(lambda:self.view.controller.createAccountActionListener(self.username.text(), self.password.text(), "name", self.phone.text(), self.email.text(), "room_number", "medical_condition"))
        print("set_act_list")
    def getWidgets(self):
        return self.widget

    def createWidgets(self):
        self.title.setObjectName("title")
        self.title.setText("CREATE ACCOUNT")
        self.title.setStyleSheet("#title{font-weight: bold; font: 'Times New Roman'; font-size: 75px; color: #282828;}")
        self.title.setStyleSheet("font-size:30pt;")
        self.title.setFixedSize(400,200)
        self.name_lbl.setText("Full Name: ")
        self.name_lbl.setStyleSheet("font-size:20pt")
        self.name.setStyleSheet("background-color:white;border-radius:20px;border: 2px solid black; font: 26pt 'Times New Roman';")
        self.name.setPlaceholderText("FirstName LastName")
        self.name.setFixedSize(400, 50)
        self.username_lbl.setText("Username: ")
        self.username_lbl.setStyleSheet("font-size:20pt")
        self.username.setStyleSheet("background-color:white;border-radius:20px;border: 2px solid black; font: 26pt 'Times New Roman';")
        self.username.setFixedSize(400, 50)
        self.username.setPlaceholderText("Username")
        self.password_lbl.setText("Password: ")
        self.password_lbl.setStyleSheet("font-size:20pt")
        self.password.setFixedSize(400, 50)
        self.password.setStyleSheet("background-color:white;border-radius:20px;border: 2px solid black; font: 26pt 'Times New Roman';")
        self.email_lbl.setText("Email Address: ")
        self.email_lbl.setStyleSheet("font-size:20pt")
        self.email.setFixedSize(400,50)
        self.email.setPlaceholderText("e.g. email@address.com")
        self.email.setStyleSheet("background-color:white;border-radius:20px;border: 2px solid black; font: 26pt 'Times New Roman';")
        self.phone_lbl.setText("Phone Number: ")
        self.phone_lbl.setStyleSheet("font-size:20pt")
        self.phone.setFixedSize(400, 50)
        self.phone.setStyleSheet("background-color:white;border-radius:20px;border: 2px solid black; font: 26pt 'Times New Roman';")
        self.phone.setPlaceholderText("e.g. XXX-XXX-XXXX")
        self.room_lbl.setText("Room Number: ")
        self.room_lbl.setStyleSheet("font-size:20pt")
        self.room.setStyleSheet("background-color:white;border-radius:20px;border: 2px solid black; font: 26pt 'Times New Roman';")
        self.room.setFixedSize(400, 50)
        self.medicalCond_lbl.setText("Medical Condition: ")
        self.medicalCond_lbl.setStyleSheet("font-size:20pt")
        self.medicalCond.setStyleSheet("background-color:white;border-radius:20px;border: 2px solid black; font: 26pt 'Times New Roman';")
        self.medicalCond.setFixedSize(400, 50)
        self.gender_lbl.setText("Gender: ")
        self.gender_lbl.setStyleSheet("font-size:20pt")
        self.male.setText("Male ")
        self.female.setText("Female ")
        self.other.setText("Other ")
        self.noAns.setText("Prefer not to answer")
        self.accType_lbl.setText("Account Type: ")
        self.family.setText("Family ")
        self.patient.setText("Patient ")
        self.admin.setText("Admin ")
        self.gender.setStyleSheet("background-color:white;border-radius:20px;border: 2px solid black; font: 28pt 'Times New Roman';")
        self.gender.setFixedSize(400, 50)
        self.accType.setStyleSheet("background-color:white;border-radius:20px;border: 2px solid black; font: 28pt 'Times New Roman';")
        self.accType.setFixedSize(400, 50)
        self.dob_lbl.setText("Date of Birth: ")
        self.dob.setPlaceholderText("YYYY/MM/DD")
        self.dob.setStyleSheet("background-color:white;border-radius:20px;border: 2px solid black; font: 28pt 'Times New Roman';")
        self.dob.setFixedSize(400, 50)

        self.name_hbox.addWidget(self.name_lbl, alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.name_hbox.addWidget(self.name, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.user_hbox.addWidget(self.username_lbl,alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.user_hbox.addWidget(self.username,alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.pass_hbox.addWidget(self.password_lbl,alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.pass_hbox.addWidget(self.password,alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.email_hbox.addWidget(self.email_lbl, alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.email_hbox.addWidget(self.email, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.phone_hbox.addWidget(self.phone_lbl, alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.phone_hbox.addWidget(self.phone, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.room_hbox.addWidget(self.room_lbl, alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.room_hbox.addWidget(self.room, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.medicalCond_hbox.addWidget(self.medicalCond_lbl, alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.medicalCond_hbox.addWidget(self.medicalCond, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.dob_hbox.addWidget(self.dob_lbl, alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.dob_hbox.addWidget(self.dob, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.gender_hbox.addWidget(self.gender_lbl, alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.gender_hbox.addWidget(self.male, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.gender_hbox.addWidget(self.female, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.gender_hbox.addWidget(self.other, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.gender_hbox.addWidget(self.noAns, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.accType_hbox.addWidget(self.accType_lbl, alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.accType_hbox.addWidget(self.patient, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.accType_hbox.addWidget(self.admin, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.accType_hbox.addWidget(self.family, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)

        self.input_var_vbox.addLayout(self.name_hbox)
        self.input_var_vbox.addLayout(self.user_hbox)
        self.input_var_vbox.addLayout(self.pass_hbox)
        self.input_var_vbox.addLayout(self.email_hbox)
        self.input_var_vbox.addLayout(self.phone_hbox)
        self.input_var_vbox.addLayout(self.room_hbox)
        self.input_var_vbox.addLayout(self.medicalCond_hbox)

        # self.input_var_vbox.addLayout(self.dob_hbox)
        # self.input_var_vbox.addLayout(self.gender_hbox)
        # self.input_var_vbox.addLayout(self.accType_hbox)
        self.input_var_vbox.setAlignment(QtCore.Qt.AlignmentFlag.AlignHCenter)

        self.input_var_vbox.setSpacing(15)

        self.finish.setText("Create Account")
        self.finish.setFixedSize(350,50)
        self.finish.setStyleSheet("background-color:#46CFEA;color:black;font-size:28pt;border-radius:25px;")

        self.back.setText("Cancel")
        self.back.setFixedSize(350, 50)
        self.back.setStyleSheet("background-color:#46CFEA;color:black;font-size:28pt;border-radius:25px;")

        self.btn_vbox.addWidget(self.finish,alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.btn_vbox.addWidget(self.back,alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.btn_vbox.setSpacing(15)

        self.vbox.addWidget(self.title, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.vbox.addLayout(self.input_var_vbox)
        self.vbox.addLayout(self.btn_vbox)


        self.panel.setLayout(self.vbox)
        self.panel.setFixedSize(800,900)
        self.panel.setStyleSheet("background-color:white;border-radius:50px;")
        self.frame_vbox.addWidget(self.panel, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)


        frame = QWidget()

        frame.setStyleSheet("background-color:#212121;")
        frame.setLayout(self.frame_vbox)
        return frame