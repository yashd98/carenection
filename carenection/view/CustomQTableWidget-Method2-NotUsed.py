#!/usr/bin/env python

import sys
from PyQt5.QtWidgets import *

#Not using this class, considered backup
#Change File name to "CustomQTableWidget" when using this class
class CustomQTableWidget(QTableWidget):

    def __init__(self, parent=None):
        QTableWidget.__init__(self, parent)

    #TODO: Edit code to liking when using this class
    def contextMenuEvent(self, event):
        # menu = QMenu(self)
        # insertabove = QAction("Insert Above", self)
        # insertbelow = QAction("Insert Below", self)
        # edit = QAction("Edit", self)
        #
        # #triggers/click events
        # insertabove.triggered.connect(self.ine)
        # menu.exec_(self.mapToGlobal(event.pos()))
        pass


#Standalone runnable test


# def logbook_add_clicked(table):
#     #TODO: call to controller
#     rowpos = table.rowCount()
#     table.insertRow(rowpos)
#     print("row inserted-", rowpos)
#
#     table.setItem(rowpos, 1, QTableWidgetItem())
#     table.setItem(rowpos, 2, QTableWidgetItem())
#     table.setItem(rowpos, 3, QTableWidgetItem())
#     table.setItem(rowpos, 4, QTableWidgetItem())
#     table.setItem(rowpos, 5, QTableWidgetItem())
#
# app = QApplication([])
# tableWidget = CustomQTableWidget()
# tableWidget.setColumnCount(6)
# columns = [None, "Patient ID", "Meal", "Medicine", "Administrator",
#            "Date & Time"]  # using '&&' to display '&' as its escape character
# tableWidget.setHorizontalHeaderLabels(columns)
# tableWidget.setSelectionBehavior(QAbstractItemView.SelectionBehavior.SelectRows)
# tableWidget.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers | QAbstractItemView.EditTrigger.DoubleClicked)
# header = tableWidget.horizontalHeader()
# header.setSectionResizeMode(0,QHeaderView.ResizeMode.ResizeToContents)
# header.setSectionResizeMode(1,QHeaderView.ResizeMode.ResizeToContents)
# header.setSectionResizeMode(2,QHeaderView.ResizeMode.Stretch)
# header.setSectionResizeMode(3,QHeaderView.ResizeMode.Stretch)
# header.setSectionResizeMode(4,QHeaderView.ResizeMode.ResizeToContents)
# header.setSectionResizeMode(5,QHeaderView.ResizeMode.ResizeToContents)
#
# logbook_add_clicked(tableWidget)
# logbook_add_clicked(tableWidget)
# logbook_add_clicked(tableWidget)
# logbook_add_clicked(tableWidget)
# logbook_add_clicked(tableWidget)
#
# tableWidget.show()
# sys.exit(app.exec_())