import os

from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtWidgets import *
from PyQt5.Qt import Qt

from models.ContactModel import ContactModel
from models.CustomContactListItem import CustomContactListItem
from models.CustomDisplayContactLabel import CustomDisplayContactLabel


class ContactsAndCallManTabContainer(QWidget):
    def __init__(self, controller):
        super(ContactsAndCallManTabContainer, self).__init__()
        self.controller = controller
        self.main_lay = QVBoxLayout(self)
        self.main_lay.setContentsMargins(0, 0, 0, 0)
        self.empty_wid = QFrame()
        self.addbtn = QPushButton()
        self.contacts_list = QListWidget()
        self.display_contact = CustomDisplayContactLabel()

        style = """
            QFrame{
                background-color: #212121; 
            }
            QFrame#separatorline{
                background-color: white;
                
            }
            QListWidget{
                background-color: #212121;
            }
            QPushButton{
                background-color: #46CFEA;
                color: #212121;
                font-size: 20px;
                border: 2px solid #212121;
                border-radius: 16px;
            }
            QPushButton:hover{
                border: 2px solid white;
            }
            QPushButton:focus{
                outline: none; /* removes extra dotted rectangle */
            }
            
        """
        self.empty_wid.setStyleSheet(style)
        self.create_widgets()
        self.main_lay.addWidget(self.empty_wid)

    def create_widgets(self):
        hlayout = QHBoxLayout()
        hlayout.setContentsMargins(0, 0, 0, 0)
        vlayout = QVBoxLayout()
        vlayout.setContentsMargins(5, 10, 5, 5)

        curdir = os.path.dirname(os.getcwd())
        pmap_add = QPixmap(os.path.join(curdir, "view/icons/add_2.png"))
        resize_height = self.addbtn.sizeHint().height() * (1 + 0.50)
        self.addbtn.setFixedSize(resize_height, resize_height)
        self.addbtn.setIcon(QIcon(pmap_add))
        self.addbtn.setToolTip("Click here to add a new contact")
        self.addbtn.clicked.connect(lambda : self.dialog_popup(1))
        vlayout.addWidget(self.addbtn, alignment=Qt.AlignRight)

        # self.contacts_list.set
        self.contacts_list.itemClicked.connect(self.contactSelected)
        self.contacts_list.setToolTip("All contacts for the current user/patient")
        vlayout.addWidget(self.contacts_list)
        hlayout.addLayout(vlayout)

        vline_separator = QFrame()
        vline_separator.setGeometry(320, 150, 118, 3)
        vline_separator.setObjectName("separatorline")
        vline_separator.setFrameShape(QFrame.VLine)
        vline_separator.setFrameShadow(QFrame.Sunken)
        hlayout.addWidget(vline_separator)

        # self.display_contact.hide() # dont show anything at the start
        self.display_contact.hide_widgets()
        # self.display_contact.show_widgets()
        self.display_contact.setObjectName("customlabel")
        self.display_contact.setToolTip("Selected contact information is displayed here")
        hlayout.addWidget(self.display_contact)

        self.empty_wid.setLayout(hlayout)

    def contactSelected(self):
        self.display_contact.setData(self.getSelectedContact().model.getData())
        self.display_contact.show_widgets()

    def addToList(self, source, model):
        # print(model)
        # call to backend to add new contact to the db
        citem = CustomContactListItem(model,1)
        item = QListWidgetItem(self.contacts_list)#, self.contacts_list)
        citem.btnedit.clicked.connect(lambda: self.contactListItemEdited({"widget":citem, "listwidgetitem":item}))
        citem.btnremove.clicked.connect(lambda: self.contactListItemRemoved({"widget":citem, "listwidgetitem":item}))
        item.setSizeHint(citem.sizeHint())
        self.contacts_list.addItem(item)
        self.contacts_list.setItemWidget(item, citem)
        source.close() #dialog box close

    def updateToList(self, source, newmodel, itemsources):
        #old model can be extracted from the itemsources["widget"]
        #compare old and new model and if there are changes then call to backend to modify existing contact
        widget = itemsources["widget"]
        # print("hello")
        oldmodel = widget.model
        widget.model = newmodel
        widget.refreshContactLabel()
        source.close()

    def getSelectedContact(self):
        sel_row = self.contacts_list.selectionModel().selectedRows()[0]
        item = self.contacts_list.item(sel_row.row())
        # item_widget = self.contacts_list.itemWidget(item)
        # print("sel_row: ", sel_row.row(), " item: ", item, " item widget: ", item_widget)
        return self.contacts_list.itemWidget(item)

    def contactListItemRemoved(self, sources):
        #call to backend to remove the selected contact from the db
        # print("Remove:: Source: ",sources)
        # print("index-0: ", self.contacts_list.item(0))
        index = self.contacts_list.indexFromItem(sources["listwidgetitem"])
        # print("index: ", index.row())
        self.contacts_list.takeItem(index.row())


    def contactListItemEdited(self, sources):
        # print("Edited:: Source: ", sources)
        self.dialog_popup(0,sources)

    def dialog_popup(self, mode, itemsources=None):
        # mode: 1-add, 0-edit

        dialog_box = QDialog()
        dialog_box.setWindowTitle("Add Contact" if mode else "Edit Contact")
        title = QLabel(dialog_box)
        # print(dialog_box.windowTitle().title())
        title.setText(dialog_box.windowTitle().title())
        title.setObjectName("title")

        hlayout_name = QHBoxLayout()
        hlayout_email = QHBoxLayout()
        hlayout_relation = QHBoxLayout()
        hlayout_actions = QHBoxLayout()
        vlayout = QVBoxLayout(dialog_box)
        vlayout.addWidget(title, alignment=Qt.AlignHCenter)

        namelbl = QLabel("Name: ")
        nametxt = QLineEdit()
        nametxt.setToolTip("Enter a name to add/update")
        hlayout_name.addWidget(namelbl)
        hlayout_name.addWidget(nametxt)

        emaillbl = QLabel("Email: ")
        emailtxt = QLineEdit()
        emailtxt.setToolTip("Enter an email to add/update")
        hlayout_email.addWidget(emaillbl)
        hlayout_email.addWidget(emailtxt)

        relationlbl = QLabel("Relation: ")
        relationtxt = QLineEdit()
        relationtxt.setToolTip("Enter a relation to add/update")
        hlayout_relation.addWidget(relationlbl)
        hlayout_relation.addWidget(relationtxt)

        addbtn = QPushButton("Add")
        addbtn.setObjectName("add-updatebtn")
        addbtn.setToolTip("Click to add the contact")

        updatebtn = QPushButton("Update")
        updatebtn.setObjectName("add-updatebtn")
        updatebtn.setToolTip("Click to update the contact")

        if mode:
            addbtn.clicked.connect(
                lambda: self.addToList(dialog_box, ContactModel(nametxt.text(), emailtxt.text(), relationtxt.text())))
            hlayout_actions.addWidget(addbtn)
        else:
            model = itemsources["widget"].model

            updatebtn.clicked.connect(
                lambda: self.updateToList(dialog_box, ContactModel(nametxt.text(), emailtxt.text(), relationtxt.text()), itemsources))
            hlayout_actions.addWidget(updatebtn)

            #fill the textbox with the edited item information
            nametxt.setText(model.getName())
            emailtxt.setText(model.getEmail())
            relationtxt.setText(model.getRelation())


        cancelbtn = QPushButton("Cancel")
        cancelbtn.setObjectName("cancelbtn")
        cancelbtn.setToolTip("Click to cancel adding the contact")
        cancelbtn.clicked.connect(lambda: dialog_box.close())
        hlayout_actions.addWidget(cancelbtn)

        vlayout.addLayout(hlayout_name)
        vlayout.addLayout(hlayout_email)
        vlayout.addLayout(hlayout_relation)
        vlayout.addLayout(hlayout_actions)

        style = """
            QDialog{
                background-color: #212121;
            }
            QLineEdit{
                font-size: 20px;
            }
            QLabel{
                color: white;
                font-size: 20px;
            }
            QLabel#title{
                color: white;
                /* border: 2px solid white; */
                padding: 10px 10px 10px 10px;
                font-size: 25px;
            }
            QPushButton#add-updatebtn{
                background-color: #46CFEA;
                color: #212121;
                font-size: 20px;
                border: 4px solid #212121;
                border-radius: 10px;
            }
            
            QPushButton#add-updatebtn:hover{
                border: 2px solid white;
            }
            
            QPushButton#cancelbtn{
                background-color: lightgrey;
                color: #212121;
                font-size: 20px;
                border: 4px solid #212121;
                border-radius: 10px;
            }
            
            QPushButton#cancelbtn:hover{
                border: 2px solid white;
            }
        """
        dialog_box.setStyleSheet(style)

        dialog_box.exec_()
