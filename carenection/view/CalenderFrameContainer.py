from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5 import QtCore
import datetime as dt

from models.PatientDayEventModel import PatientDayEventModel


class CalenderFrameContainer:
    def __init__(self, controller):
        super(CalenderFrameContainer, self).__init__()
        self.controller = controller
        self.date_after_week = dt.datetime
        self.hbox_layout = QHBoxLayout()
        self.left_vbox = QVBoxLayout()
        self.weekOf = QLabel()
        self.weekList = QListWidget()
        self.calender = QCalendarWidget()
        self.vbox_weekList = QVBoxLayout()
        self.leftWidget = QWidget()
        self.qwidget = QWidget()
        self.patient_day_event_model_list = []
        self.widget = self.createCalenderWidgets()

    def getCalenderWidgets(self):
        return self.widget

    def createCalenderWidgets(self):

        self.weekOf.setAlignment(QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.weekOf.setStyleSheet("color:black;font-size:20 pt;text-decoration: underline;font-size: 30px;")
        self.calender.setGridVisible(True)
        self.calender.setFont(QFont('Times', 15))
        self.calender.setStyleSheet(self.getCalendarStylesheet())
        self.weekList.setStyleSheet("QListWidget{background-color:#212121}QListWidget::item:selected{background-color:rgba(18,156,211,.4);}")
        self.updateCalendarFrameContainer()
        self.left_vbox.addWidget(self.weekOf)
        self.left_vbox.addWidget(self.weekList)
        self.hbox_layout.addLayout(self.left_vbox)
        self.hbox_layout.addWidget(self.calender)
        self.qwidget.setLayout(self.hbox_layout)
        return self.qwidget

    def getWeekOf(self):
        today = dt.datetime.today()
        weekFromToday = today + dt.timedelta(days=6)
        return "Week from " + today.strftime("%b") + " " + today.strftime("%d") + " - " + weekFromToday.strftime(
            "%b") + " " + weekFromToday.strftime("%d")

    def updateCalendarFrameContainer(self):
        self.weekOf.setText(self.getWeekOf())
        self.date_after_week = dt.datetime.today() + dt.timedelta(days=7)
        if self.weekList.count() > 0:
            self.weekList.clear()
            self.patient_day_event_model_list.clear()

        for x in range(0, 7):
            patient_day_event_model = PatientDayEventModel(self.controller, dt.datetime.today() + dt.timedelta(days=x))
            self.patient_day_event_model_list.append(patient_day_event_model)
            item = QListWidgetItem(self.weekList)
            item.setSizeHint(patient_day_event_model.getWidgets().sizeHint())
            self.weekList.addItem(item)
            self.weekList.setItemWidget(item, patient_day_event_model.getWidgets())
            if len(patient_day_event_model.events) == 0:
                self.weekList.item(x).setHidden(True)

        if self.weekList.count()>0:
            self.weekList.item(0).setSelected(True)
        self.weekList.setFocus()

    def updateSpecificEventModel(self, date):

        for x in range(0, len(self.weekList)):
            model_date = dt.datetime.strptime(self.weekList.children()[0].children()[x].children()[1].text(),
                                              "%A, %b %d, %Y")

            if model_date.date() == date.date():

                self.patient_day_event_model_list[x].updateEventModels()

                self.weekList.item(x).setSizeHint(self.patient_day_event_model_list[x].getWidgets().sizeHint())
                if self.weekList.item(x).isHidden():
                    self.weekList.item(x).setHidden(False)
                elif self.patient_day_event_model_list[x].inner_event_vbox.count()==0:
                    self.weekList.item(x).setHidden(True)

    def getCalendarStylesheet(self):
        return "QAbstractItemView{selection-background-color: #46CFEA;selection-color: black;}"