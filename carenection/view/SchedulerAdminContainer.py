from datetime import datetime
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from models.AdminUIEventModel import AdminUIEventModel


class SchedulerAdminContainer(QWidget):
    def __init__(self, controller, calendar_stacked_widget, view):
        super(SchedulerAdminContainer, self).__init__()
        self.date = None
        self.controller = controller
        self.calendar_stacked_widget = calendar_stacked_widget
        self.view = view
        self.events_object_list = []
        self.events = []
        self.outer_vlayout = QVBoxLayout()
        self.back_btn = QPushButton()
        self.date_lbl = QLabel()
        self.add_del_evt_layout = QHBoxLayout()
        self.add_btn = QPushButton()
        self.delete_btn = QPushButton()
        self.event_list = QListWidget()
        self.event_list_counter = 0
        self.inner_vlayout = QVBoxLayout()
        self.layoutWidget = QWidget()
        self.scroll_widget = QWidget()
        self.scrollArea = QScrollArea()
        self.back_btn.clicked.connect(lambda: self.back_btn_listener())
        self.add_btn.clicked.connect(lambda: self.add_btn_listener())
        self.schedulerWidget = self.createSchedulerContainer()

    def getSchedulerContainer(self):
        return self.schedulerWidget

    def createSchedulerContainer(self):
        self.back_btn.setText("Back")
        self.back_btn.setFixedSize(200, 50)
        self.back_btn.setStyleSheet(self.getBtnStyleSheet())
        self.back_btn.setToolTip("Back to calendar")
        self.date_lbl.setText("Monday, November 5th, 2021")
        self.date_lbl.setAlignment(Qt.AlignmentFlag.AlignHCenter)
        self.date_lbl.setToolTip("Selected Date")
        self.date_lbl.setStyleSheet("font:30pt;color:white;")
        self.add_btn.setText(" + ")
        self.add_btn.setFixedSize(50, 50)
        self.add_btn.setToolTip("Add a new event")
        self.add_btn.setStyleSheet(self.getBtnStyleSheet())

        self.add_del_evt_layout.addWidget(self.add_btn)
        self.add_del_evt_layout.setAlignment(Qt.AlignmentFlag.AlignRight)

        self.event_list.setSpacing(10)
        self.inner_vlayout.addWidget(self.event_list)

        self.outer_vlayout.addWidget(self.back_btn)
        self.outer_vlayout.addWidget(self.date_lbl)
        self.outer_vlayout.addLayout(self.add_del_evt_layout)
        self.outer_vlayout.addLayout(self.inner_vlayout)
        self.layoutWidget.setLayout(self.outer_vlayout)

        return self.layoutWidget

    def back_btn_listener(self):

        for x in range(len(self.event_list)-1,-1):
            self.event_list.takeItem(x)
            del self.events_object_list[x]

        self.calendar_stacked_widget.setCurrentIndex(0)

    def setWidgets(self, date):
        self.date = date
        self.date_lbl.setText(date.strftime("%A, %b %d"))
        self.events = self.controller.cal_adm_dt_sel_listener(date)
        self.event_list_counter = len(self.event_list)
        for ev in self.events:
            event_widget = AdminUIEventModel(self, ev, self.event_list_counter)
            self.events_object_list.append(event_widget)
            item = QListWidgetItem(self.event_list)
            item.setSizeHint(event_widget.getWidget().sizeHint())
            self.event_list.addItem(item)
            self.event_list.setItemWidget(item, event_widget.getWidget())

    def add_btn_listener(self):
        dialog_box = QDialog()
        dialog_box.setWindowTitle("Add Events")
        vbox_layout = QVBoxLayout(dialog_box)
        title = QLabel(dialog_box)
        event_name_hbox = QHBoxLayout(dialog_box)
        event_description_hbox = QHBoxLayout(dialog_box)
        event_start_time_hbox = QHBoxLayout(dialog_box)
        event_end_time_hbox = QHBoxLayout(dialog_box)
        event_name_lbl = QLabel(dialog_box)
        event_description_lbl = QLabel(dialog_box)
        event_start_time_lbl = QLabel(dialog_box)
        event_end_time_lbl = QLabel(dialog_box)
        event_name_textfield = QLineEdit(dialog_box)
        event_description_textfield = QLineEdit(dialog_box)
        event_start_time_drop = QComboBox(dialog_box)
        event_end_time_drop = QComboBox(dialog_box)
        done_btn = QPushButton(dialog_box)

        self.createComboItems(event_start_time_drop, event_end_time_drop)
        event_start_time_drop.setStyleSheet("background-color:white;color:black")
        event_end_time_drop.setStyleSheet("background-color:white;color:black")

        title.setText("Add Events")
        title.setAlignment(Qt.AlignmentFlag.AlignHCenter)
        event_name_lbl.setText("Event Name : ")
        event_description_lbl.setText("Description : ")
        event_start_time_lbl.setText("Start Time : ")
        event_end_time_lbl.setText("End Time : ")
        done_btn.setText("Done")
        done_btn.clicked.connect(
            lambda: self.add_event_done_listener(dialog_box, self.getTimeObject(event_start_time_drop.currentText()),
                                                 self.getTimeObject(event_end_time_drop.currentText()),
                                                 event_description_textfield.text(), event_name_textfield.text()))
        event_name_textfield.setStyleSheet("background-color:white;color:black")
        event_description_textfield.setStyleSheet("background-color:white;color:black")

        event_name_hbox.addWidget(event_name_lbl)
        event_name_hbox.addWidget(event_name_textfield)
        event_description_hbox.addWidget(event_description_lbl)
        event_description_hbox.addWidget(event_description_textfield)
        event_start_time_hbox.addWidget(event_start_time_lbl)
        event_start_time_hbox.addWidget(event_start_time_drop)
        event_end_time_hbox.addWidget(event_end_time_lbl)
        event_end_time_hbox.addWidget(event_end_time_drop)

        vbox_layout.addWidget(title)
        vbox_layout.addLayout(event_name_hbox)
        vbox_layout.addLayout(event_description_hbox)
        vbox_layout.addLayout(event_start_time_hbox)
        vbox_layout.addLayout(event_end_time_hbox)
        vbox_layout.addWidget(done_btn)

        # stylesheet
        dialog_box.setStyleSheet("background-color: #212121; color: white; font-size: 25px;")
        done_btn.setStyleSheet("background-color:#46CFEA; font-size: 30px;color:#212121;border:4px solid #212121; border-radius:20px;}QPushButton:hover{border: 4px solid white;")
        dialog_box.exec()

    def getBtnStyleSheet(self):
        return "QPushButton{background-color:#46CFEA; font-size: 20px; color:#212121;border: 4px solid #212121; border-radius:25px;}QPushButton:hover{" \
               "border: 4px solid white;} "

    def getTimeObject(self, string):
        return datetime.strptime(str(string), "%I %p").time()

    def createComboItems(self, start_time_drop, end_time_drop):
        am = "AM"
        pm = "PM"
        start_time_drop.addItem("12 AM")
        end_time_drop.addItem("12 AM")
        for x in range(1, 12):
            time = str(x) + " " + am
            start_time_drop.addItem(time)
            end_time_drop.addItem(time)

        start_time_drop.addItem("12 PM")
        end_time_drop.addItem("12 PM")

        for y in range(1, 12):
            time = str(y) + " " + pm
            start_time_drop.addItem(time)
            end_time_drop.addItem(time)

    def add_event_done_listener(self, dialog_box, event_start_time_textfield, event_end_time_textfield,
                                event_description_textfield,
                                event_name_textfield):

        self.event_list_counter = len(self.event_list)
        #date = datetime.strptime(self.date_lbl, '%a %b %d %Y')
        start_dt = datetime.combine(self.date, event_start_time_textfield)
        end_dt = datetime.combine(self.date, event_end_time_textfield)
        event = self.controller.cal_adm_add_event_listener(3, start_dt, end_dt,
                                                           event_description_textfield, event_name_textfield)
        event_widget = AdminUIEventModel(self, event, self.event_list_counter)

        self.events_object_list.append(event_widget)

        item = QListWidgetItem(self.event_list)
        item.setSizeHint(event_widget.getWidget().sizeHint())
        self.event_list.addItem(item)
        self.event_list.setItemWidget(item, event_widget.getWidget())

        self.view.calenderFrame.updateSpecificEventModel(self.date)
        dialog_box.close()

    def delete_event(self, index):
        self.controller.delete_event_listener(self.events_object_list[index].event_model)
        for x in range(index + 1, len(self.event_list)):
            self.events_object_list[x].updateIndex()

        if 0 <= index < len(self.event_list):
            self.event_list.takeItem(index)
            del self.events_object_list[index]

        self.view.calenderFrame.updateSpecificEventModel(self.date)
