import os

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5 import QtCore

from models.QAnimatedStackedWidget import QAnimatedStackedWidget


class PictureFrameView(QMainWindow):
    def __init__(self, controller):
        super(PictureFrameView, self).__init__()
        self.controller = controller
        self.slideshow = QAnimatedStackedWidget()

    def getSlideShow(self):
        return self.slideshow

    def setSlideShow(self, slideshow):
        self.slideshow = slideshow
