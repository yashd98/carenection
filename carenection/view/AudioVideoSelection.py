import os

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5 import QtCore
from PyQt5.Qt import Qt

from models.CustomDisplayContactLabel import CustomDisplayContactLabel


class AudioVideoSelection(QWidget):
    def __init__(self, stackedframes):
        super(AudioVideoSelection, self).__init__()
        self.stackedframes = stackedframes
        # self.main_lay = QVBoxLayout(self)
        # self.main_lay.setContentsMargins(0, 0, 0, 0)
        self.empty_wid = QWidget()
        self.contactLabel = CustomDisplayContactLabel()
        self.backbtn = QPushButton("Back")
        self.audiobtn = QPushButton("Audio Call")
        self.videobtn = QPushButton("Video Call")
        self.createWidgets()
        # self.main_lay.addWidget(self.empty_wid)
        style="""
            QWidget{
                background-color: #212121;
            }
            QPushButton{
                background-color: #46CFEA;
                color: #212121;
                font-size: 30px;
                border: 2px solid #212121;
                border-radius: 20px;
            }
            QPushButton:focus{
                border: 2px solid white;
                border-radius: 20px;
                outline: none;
            }
        """
        self.empty_wid.setStyleSheet(style)
    def getWidgets(self):
        return self.empty_wid

    def createWidgets(self):
        vlayout = QVBoxLayout()
        hlayout_actions = QHBoxLayout()

        self.backbtn.clicked.connect(lambda :self.backSelected())
        self.audiobtn.clicked.connect(self.audioCallSelected)
        self.videobtn.clicked.connect(self.videoCallSelected)
        hlayout_actions.addWidget(self.backbtn)
        hlayout_actions.addWidget(self.audiobtn)
        hlayout_actions.addWidget(self.videobtn)

        vlayout.addWidget(self.contactLabel, alignment=Qt.AlignHCenter)
        vlayout.addLayout(hlayout_actions)

        self.empty_wid.setLayout(vlayout)

    def updateInfo(self, data):
        self.contactLabel.setData(data)

    def backSelected(self):
        print("AUDVIDSEL")
        self.stackedframes.setCurrentIndex(0)

    def audioCallSelected(self):
        dialog_box = QDialog()
        dialog_box.setWindowTitle("Coming soon...")
        title = QLabel()
        title.setText(dialog_box.windowTitle().title())
        title.setObjectName("title")

        vlayout = QVBoxLayout(dialog_box)
        vlayout.addWidget(title, alignment=Qt.AlignCenter)

        actions = dialog_box.exec_()

    def videoCallSelected(self):
        dialog_box = QDialog()
        dialog_box.setWindowTitle("Coming soon...")
        title = QLabel()
        title.setText(dialog_box.windowTitle().title())
        title.setObjectName("title")

        vlayout = QVBoxLayout(dialog_box)
        vlayout.addWidget(title, alignment=Qt.AlignCenter)

        actions = dialog_box.exec_()
