from datetime import datetime

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from view.SchedulerAdminContainer import SchedulerAdminContainer

class CalendarAdminContainer(QWidget):
    def __init__(self, controller, calendar_stacked_widget, view):
        super(CalendarAdminContainer, self).__init__()
        self.controller = controller
        self.calendar_stacked_widget = calendar_stacked_widget
        self.view = view
        self.events = []
        self.cal = QCalendarWidget()
        self.cal.setStyleSheet("background-color: white; color: #212121;")
        self.layout = QVBoxLayout()
        self.layoutWidget = QWidget()
        self.scheduler_admin_container = SchedulerAdminContainer(self.controller, self.calendar_stacked_widget, self.view)
        self.cal.activated.connect(lambda: self.date_selected())
        self.calendar = self.createCalendarWidgets()
        self.calendar_stacked_widget.addWidget(self.getCalendarWidget())
        self.calendar_stacked_widget.addWidget(self.scheduler_admin_container.getSchedulerContainer())
        self.calendar_stacked_widget.setStyleSheet("background-color:#212121;")

    def getCalendarWidget(self):
        return self.calendar

    def createCalendarWidgets(self):
        self.cal.setGridVisible(True)
        self.cal.setFont(QFont('Times', 15))
        self.layout.addWidget(self.cal)
        self.layoutWidget.setLayout(self.layout)
        return self.layoutWidget

    def date_selected(self):
        date = self.cal.selectedDate().toPyDate()
        date = datetime.combine(date, datetime.min.time())
        self.scheduler_admin_container.setWidgets(date)
        self.calendar_stacked_widget.setCurrentIndex(1)
