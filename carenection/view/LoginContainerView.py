import PyQt5
from PyQt5.QtWidgets import *
from PyQt5 import QtCore
from PyQt5.QtGui import *
import os


class LoginContainerView(QMainWindow):
    def __init__(self, controller):
        super(LoginContainerView, self).__init__()
        self.controller = controller
        self.icon = QLabel()
        self.login_btn = QPushButton("Login")
        self.create_btn = QPushButton("Create Account")
        self.v_layout = QVBoxLayout()
        self.user_layout = QHBoxLayout()
        self.pass_layout = QHBoxLayout()
        self.user_pass_v_layout = QVBoxLayout()
        self.user_pass_v_layout_widget = QWidget()
        self.username_lbl = QLabel("Username : ")
        self.username_text = QLineEdit()
        self.password_lbl = QLabel(" Password : ")
        self.password_text = QLineEdit()
        self.v_cont = QWidget()
        self.login_container = self.createLoginWidgets()
        self.setActionListener()

    def getLoginContainer(self):
        return self.login_container

    def setActionListener(self):
        self.login_btn.clicked.connect(lambda: self.controller.loginActionListener(self.username_text.text(), self.password_text.text()))
        self.create_btn.clicked.connect(self.controller.createActionListener)

    def createLoginWidgets(self):
        file_dir = os.path.dirname(os.path.realpath(__file__))
        file_name = os.path.join(file_dir, "icons/LoginLogo.png")

        self.icon.setPixmap(QPixmap(file_name))
        self.icon.setScaledContents(True)
        self.icon.setFixedSize(900, 420)

        self.username_lbl.setStyleSheet("font: 35pt 'Times New Roman';color:white;")

        self.username_text.setStyleSheet("background-color:white;font: 28pt 'Times New Roman';border-radius:20px;")

        self.user_layout.addWidget(self.username_lbl)
        self.user_layout.addWidget(self.username_text)

        self.password_lbl.setStyleSheet("font: 35pt 'Times New Roman';color:white;")

        self.password_text.setEchoMode(QLineEdit.Password)
        self.password_text.setStyleSheet("background-color:white;font: 28pt 'Times New Roman'; border-radius:20px;")

        self.pass_layout.addWidget(self.password_lbl)
        self.pass_layout.addWidget(self.password_text)


        self.login_btn.setFixedSize(350, 100)
        self.login_btn.setStyleSheet(self.setStyleSheetBtn())

        self.create_btn.setFixedSize(350, 100)
        self.create_btn.setStyleSheet(self.setStyleSheetBtn())

        self.user_pass_v_layout.setSpacing(20)
        self.user_pass_v_layout.addLayout(self.user_layout)
        self.user_pass_v_layout.addLayout(self.pass_layout)
        self.user_pass_v_layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignVCenter)

        self.user_pass_v_layout_widget.setFixedSize(1000, 500)
        self.user_pass_v_layout_widget.setLayout(self.user_pass_v_layout)

        self.v_layout.addWidget(self.icon, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.v_layout.addWidget(self.user_pass_v_layout_widget)
        self.v_layout.addWidget(self.login_btn, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.v_layout.addWidget(self.create_btn, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.v_layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.v_layout.setSpacing(20)

        self.v_cont.setStyleSheet("background-color:#212121;")
        self.v_cont.setLayout(self.v_layout)

        return self.v_cont

    def setStyleSheetBtn(self):
        qss = "QPushButton{background-color:#46CFEA;font: 28pt 'Times New Roman'; border-radius:50px;color:black;}QPushButton:pressed{background-color:white;}"
        return qss
