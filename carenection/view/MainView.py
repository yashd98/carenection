import PyQt5.QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5 import QtCore
from pathlib import *
import re
from models.QLabelClickable import QLabelClickable
import os


class MainView(QMainWindow):
    def __init__(self, controller, pictureFrame, videoFrame, calenderFrame):
        super(MainView, self).__init__()
        self.controller = controller
        self.pictureFrame = pictureFrame
        self.videoFrame = videoFrame
        self.calenderFrame = calenderFrame
        self.v_layout = QVBoxLayout()
        self.carousel = QHBoxLayout()
        self.menu = QHBoxLayout()
        self.exit = QPushButton()
        self.time = QLabel()
        self.admin_btn = QPushButton()
        self.topBanner = QWidget()
        self.bottomBanner = QWidget()
        self.pf_btn = QPushButton()
        self.vf_btn = QPushButton()
        self.cf_btn = QPushButton()
        self.mainWidget = QWidget()
        self.frameContainer = QStackedWidget()
        self.mainView = self.createMainViewWidgets()
        self.pf_btn.clicked.connect(self.pf_btn_action_listener)
        self.vf_btn.clicked.connect(self.vf_btn_action_listener)
        self.cf_btn.clicked.connect(self.cf_btn_action_listener)
        self.exit.clicked.connect(self.exit_btn_action_listener)
        self.admin_btn.clicked.connect(self.admin_btn_action_listener)

    def getMainView(self):
        return self.mainView

    def createMainViewWidgets(self):

        # First index of v_layout
        file_dir = os.path.dirname(os.path.realpath(__file__))
        file_name = os.path.join(file_dir, "icons/manager (2).png")

        self.admin_btn.setIcon(QIcon(QPixmap(file_name)))
        self.admin_btn.setFixedSize(90, 90)
        self.admin_btn.setIconSize(QtCore.QSize(self.admin_btn.height(), self.admin_btn.height()))
        self.admin_btn.setContentsMargins(QtCore.QMargins(0, 0, 0, 0))
        self.admin_btn.setStyleSheet("background-color:rgba(255,255,255,0);")

        file_name = os.path.join(file_dir, "icons/exit.png")
        self.exit.setIcon(QIcon(QPixmap(file_name)))
        self.exit.setFixedSize(80, 80)
        self.exit.setIconSize(QtCore.QSize(self.exit.height(),self.exit.height()))
        self.exit.setContentsMargins(QtCore.QMargins(0, 0, 0, 0))
        self.exit.setStyleSheet("background-color:rgba(255,255,255,0);")

        self.time.setAlignment(QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.time.setStyleSheet("color:white;font:40pt 'Times New Roman';")

        self.menu.addWidget(self.exit)
        self.menu.addWidget(self.time)
        self.menu.addWidget(self.admin_btn)

        self.topBanner.setLayout(self.menu)
        self.topBanner.setContentsMargins(QtCore.QMargins(0, 0, 0, 0))
        self.topBanner.setStyleSheet(
            "background-color: qlineargradient( x1:0 y1:0, x2:1 y2:0, stop:0 #212121, stop:1 #212121);")

        # Second index of v_layout
        #This reading configuration should be done through the controller, model and DataBase
        file=open(str(Path(__file__).parent.parent / f"view/user_config/config.txt"),"r")
        f=re.split('[,.]',file.read())
        picConfig=f[0]
        calendarConfig=f[1]
        #print(str(file.read))
        if(picConfig=="True"):
            self.frameContainer.addWidget(self.pictureFrame.getSlideShow())
        self.frameContainer.addWidget(self.videoFrame.getVideoWidgets())
        if (calendarConfig == "True"):
            self.frameContainer.addWidget(self.calenderFrame.getCalenderWidgets())


        # Third index of v_layout
        self.pf_btn.setText("Picture Frame")
        self.pf_btn.setFixedSize(500, 110)
        self.pf_btn.setStyleSheet(self.carousel_btn_stylesheet())

        self.vf_btn.setText("Video/Voice Call")
        self.vf_btn.setFixedSize(500, 110)
        self.vf_btn.setStyleSheet(self.default_carousel_btn_stylesheet())

        self.cf_btn.setText("Calendar")
        self.cf_btn.setFixedSize(500, 110)
        self.cf_btn.setStyleSheet(self.default_carousel_btn_stylesheet())

        if (picConfig == "True"):
            self.carousel.addWidget(self.pf_btn)
        self.carousel.addWidget(self.vf_btn)
        if (calendarConfig == "True"):
            self.carousel.addWidget(self.cf_btn)

        self.bottomBanner.setLayout(self.carousel)
        self.bottomBanner.setContentsMargins(QtCore.QMargins(0, 0, 0, 0))
        # self.bottomBanner.setStyleSheet("background-color: qlineargradient( x1:0 y1:0, x2:1 y2:0, stop:0 #D946EF, stop:1 #9333EA);") #D6859D
        self.bottomBanner.setStyleSheet(
            "background-color: qlineargradient( x1:0 y1:0, x2:1 y2:0, stop:0 #212121, stop:1 #212121);")  # F066AB ##F56B77

        self.v_layout.addWidget(self.topBanner)
        self.v_layout.addWidget(self.frameContainer)
        self.v_layout.addWidget(self.bottomBanner)
        self.v_layout.setContentsMargins(QtCore.QMargins(0, 0, 0, 0))
        self.v_layout.setSpacing(0)

        self.mainWidget.setLayout(self.v_layout)
        file.close()
        return self.mainWidget

    def setAppTime(self, time):
        self.time.setText(time)

    def carousel_btn_stylesheet(self):
        qss = "QPushButton{background-color:#46CFEA;color:black;border-radius:30px;font:30pt 'Times New Roman'}"
        return qss

    def default_carousel_btn_stylesheet(self):
        qss = "QPushButton{background-color:white;color:black;border-radius:30px;font:30pt 'Times New Roman'}"
        return qss

    def admin_btn_action_listener(self):
        self.controller.admin_listener()

    def exit_btn_action_listener(self):
        self.controller.exit_listener()

    def pf_btn_action_listener(self):
        self.pf_selector()
        self.controller.pictureFrameListener()

    def vf_btn_action_listener(self):
        self.vf_selector()
        self.controller.videoFrameListener()

    def cf_btn_action_listener(self):
        self.cf_selector()
        self.controller.calenderFrameListener()

    def pf_selector(self):
            self.vf_btn.setStyleSheet(self.default_carousel_btn_stylesheet())
            self.cf_btn.setStyleSheet(self.default_carousel_btn_stylesheet())
            self.pf_btn.setStyleSheet(self.carousel_btn_stylesheet())

    def cf_selector(self):
            self.vf_btn.setStyleSheet(self.default_carousel_btn_stylesheet())
            self.pf_btn.setStyleSheet(self.default_carousel_btn_stylesheet())
            self.cf_btn.setStyleSheet(self.carousel_btn_stylesheet())

    def vf_selector(self):
            self.pf_btn.setStyleSheet(self.default_carousel_btn_stylesheet())
            self.cf_btn.setStyleSheet(self.default_carousel_btn_stylesheet())
            self.vf_btn.setStyleSheet(self.carousel_btn_stylesheet())
