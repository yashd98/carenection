from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
class SettingsTabContainer(QWidget):
    def __init__(self, controller):
        super().__init__()
        self.controller = controller
        self.settingGroup = QGroupBox("Select Patient View Items")
        self.picFrame = QCheckBox("Picture Frame")
        self.calendar = QCheckBox("Calendar")
        self.pictureBtn = QPushButton("Upload Picture Frame")
        self.pictureBtn.clicked.connect(self.loadPic)
        self.applyBtn = QPushButton("Apply")
        self.applyBtn.clicked.connect(self.updateChanges)
        settingLayout = QVBoxLayout()
        self.spacer=QSpacerItem(10,500,QSizePolicy.Minimum,QSizePolicy.Expanding)
        settingLayout.addWidget(self.picFrame)
        settingLayout.addWidget(self.calendar)
        settingLayout.addItem(self.spacer)
        settingLayout.addWidget(self.pictureBtn,alignment=Qt.AlignCenter|Qt.AlignmentFlag.AlignBottom)
        settingLayout.addWidget(self.applyBtn,alignment=Qt.AlignCenter|Qt.AlignmentFlag.AlignBottom)
        settingLayout.setContentsMargins(0,0,0,0)
        #settingLayout.setSpacing(0)
        settingLayout.addStretch(1)
        self.picFrame.setStyleSheet("QCheckBox{color:white;font-size:50px;} QCheckBox::indicator{width:40px;height:40px;}")
        self.calendar.setStyleSheet("QCheckBox{color:white;font-size:50px;} QCheckBox::indicator{width:40px;height:40px;}")
        self.pictureBtn.setStyleSheet("QPushButton"
                          "{"
                          "border-radius:12px;font:25px;color:black; background-color:#46CFEA;"
                          "}"
            "QPushButton:hover"
                          "{"
                          "border:4px solid white;"
                          "}")
        self.applyBtn.setStyleSheet("QPushButton"
                          "{"
                          "border-radius:12px;font:25px;color:black; background-color:#46CFEA;"
                          "}"
            "QPushButton:hover"
                          "{"
                          "border:4px solid white;"
                          "}")
        self.pictureBtn.setFixedSize(1300,50)
        self.applyBtn.setFixedSize(1300, 50)
        self.settingGroup.setLayout(settingLayout)
        self.settingGroup.setStyleSheet("font-size:50px;color:white;background-color:#212121;")
        #212121-black
        #46CFEA-blue
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.settingGroup)
        self.setLayout(mainLayout)

    def loadPic(self):
        file=QFileDialog.getOpenFileNames()
        self.controller.uploadPicture(file[0])

    def updateChanges(self):
        self.controller.updateConfig(self.picFrame.isChecked(),self.calendar.isChecked())
        #figure out how to reload mainview
        msg=QMessageBox()
        msg.setWindowTitle("Settings Update")
        msg.setText("Please Restart Device to show updated display")
        x=msg.exec_()
        #please restart device to load display settings