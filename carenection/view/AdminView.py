from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets


from view.CalendarAndSchedularTabContainer import CalendarAndSchedularTabContainer
from view.ContactsAndCallManTabContainer import ContactsAndCallManTabContainer
from view.InstrManualTabContainer import InstrManualTabContainer
from view.LogbookTabContainer import LogbookTabContainer
from view.SettingsTabContainer import SettingsTabContainer
from pathlib import Path

class AdminView(QMainWindow):
    def __init__(self, controller, view):
        super(AdminView, self).__init__()
        self.controller = controller
        self.view = view
        self.setWindowTitle("Admin View")
        carenections_logo_path = str(Path(__file__).parent / "icons/carenections_logo.png")
        self.setWindowIcon(QIcon(carenections_logo_path))
        self.bottomBanner = QWidget()
        self.menu = QHBoxLayout()
        self.exit = QPushButton()
        self.wid = self.createAdminWidgets()
        self.exit.clicked.connect(self.exitAdminListener)


    def getAdminWidgets(self):
        return self.wid


    def createAdminWidgets(self):
        adminview = QTabWidget()
        exit_door_icon_path = str(Path(__file__).parent / "icons/exit_door_icon.png")
        settings_icon_path = str(Path(__file__).parent / "icons/settings.png")
        instr_icon_path = str(Path(__file__).parent / "icons/instruction.png")
        logbook_icon_path = str(Path(__file__).parent / "icons/logs.png")
        callMan_icon_path = str(Path(__file__).parent / "icons/video-call.png")
        scheduler_icon_path = str(Path(__file__).parent / "icons/schedule.png")
        # Exit Admin View Button --> Returns back to patient view
        self.exit.setIcon(QIcon(QPixmap(exit_door_icon_path)))
        self.exit.setFixedSize(60,60)
        self.exit.setIconSize(QtCore.QSize(self.exit.height(), self.exit.height()))
        self.exit.setContentsMargins(QtCore.QMargins(0, 0, 0, 0))
        self.exit.setStyleSheet("background-color: rgba(255,255,255,0);")
        # Menu
        self.menu.addWidget(self.exit)

        # Top Banner
        self.bottomBanner.setLayout(self.menu)
        self.bottomBanner.setContentsMargins(QtCore.QMargins(0, 0, 0, 0))
        self.bottomBanner.setStyleSheet("background-color: #212121; height: 5px; right-align: 1000px; padding: 10px")

        # Tabs

        adminview.addTab(LogbookTabContainer(self.controller), QIcon(logbook_icon_path), 'Log Manager')
        adminview.addTab(CalendarAndSchedularTabContainer(self.controller, self.view).getWidget(), QIcon(scheduler_icon_path), "Calendar && Scheduler")
        adminview.addTab(ContactsAndCallManTabContainer(self.controller), QIcon(callMan_icon_path), "Contacts && Call Manager")
        adminview.addTab(SettingsTabContainer(self.controller), QIcon(settings_icon_path), "Settings")
        adminview.addTab(InstrManualTabContainer(), QIcon(instr_icon_path), "Instruction Manual")

        adminview.setTabToolTip(0, "Log important information and notes")
        adminview.setTabToolTip(1, "Add & Edit Contacts and Schedule Audio/Video calls")
        adminview.setTabToolTip(2, "Keep Track of your Events, Appointments and Important Days")
        adminview.setTabToolTip(3, "Manage Application Settings")
        adminview.setTabToolTip(4, "Learn about different features available in this Application")
        adminview.setIconSize(QtCore.QSize(35, 35))

        # Admin Page
        vbox = QVBoxLayout()
        vbox.addWidget(adminview)
        displayWidget = QWidget()
        displayWidget.setLayout(vbox)
        vbox.addWidget(self.bottomBanner)

        # Admin Screen Design
        # displayWidget.setStyleSheet("background-color:#282828; font: bold; color:#D3D3D3; font-size:18px;")
        # Admin Screen Tab Bar Design

        adminview.setStyleSheet("QTabBar::tab{font-size: 22px;background-color:white;border-radius:20px;text-align: right;padding-left: 12px; padding-right: 12px; height:80px;width:300px;border:4px solid black;color: black} QTabBar::tab:selected{background-color: #46CFEA} QToolTip{color: black; font-size: 20px}")
        # adminview.setStyleSheet("QTabBar::tab{border-radius:50px}")
        return displayWidget


    def exitAdminListener(self):
        self.controller.exit_admin_listener()

